package com.fitfija.quest.backend.answercheckers;

import com.fitfija.quest.backend.model.domain.Answer;
import com.fitfija.quest.backend.model.domain.Task;

public class DefaultAnswerChecker implements AnswerChecker {

    @Override
    public boolean checkAnswer(Task task, String answer) {
        for (Answer correctAnswer : task.getAnswers()) {
            if (answer.equalsIgnoreCase(correctAnswer.getAnswer())) {
                return true;
            }
        }

        // если в базе для задания нет ни одного ответа, то любая попытка будет неуспешной

        return false;
    }
}
