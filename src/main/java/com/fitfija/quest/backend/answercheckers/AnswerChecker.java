package com.fitfija.quest.backend.answercheckers;

import com.fitfija.quest.backend.model.domain.Task;

public interface AnswerChecker {
    boolean checkAnswer(Task task, String answer);
}
