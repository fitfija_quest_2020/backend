package com.fitfija.quest.backend.answercheckers;

import com.fitfija.quest.backend.model.domain.Answer;
import com.fitfija.quest.backend.model.domain.Task;
import com.fitfija.quest.backend.services.ParticipantContext;
import com.fitfija.quest.backend.services.ParticipantContextProvider;
import lombok.RequiredArgsConstructor;

/**
 * Специальный класс для задания, для выполнения которого нужно отправить правильный ответ 13 раз.
 */

@RequiredArgsConstructor
public class MultipleTriesAnswerChecker implements AnswerChecker {

    private final ParticipantContextProvider participantContextProvider;

    private final int MAGIC = 1000000;

    @Override
    public boolean checkAnswer(Task task, String answer) {
        ParticipantContext participantContext = participantContextProvider.getContext();

        if (checkAnswerString(task, answer)) {
            // костыль, чтобы не создавать отдельную таблицу

            participantContext.setTaskTries(task,participantContext.getTaskTries(task) + MAGIC);
            return participantContext.getTaskTries(task) > MAGIC * 13;
        }

        return false;
    }

    private boolean checkAnswerString(Task task, String answer) {
        for (Answer correctAnswer : task.getAnswers()) {
            if (answer.equalsIgnoreCase(correctAnswer.getAnswer())) {
                return true;
            }
        }

        return false;
    }
}
