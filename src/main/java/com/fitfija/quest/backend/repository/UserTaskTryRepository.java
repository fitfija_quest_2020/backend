package com.fitfija.quest.backend.repository;

import com.fitfija.quest.backend.model.domain.Task;
import com.fitfija.quest.backend.model.domain.UserTaskTry;
import com.fitfija.quest.backend.model.domain.User;
import org.springframework.data.repository.CrudRepository;

import java.util.Optional;

public interface UserTaskTryRepository extends CrudRepository<UserTaskTry, Long> {
    Optional<UserTaskTry> findByUserAndTask(User user, Task task);
}
