package com.fitfija.quest.backend.repository;

import com.fitfija.quest.backend.model.domain.Configuration;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface ConfigurationRepository extends CrudRepository<Configuration, Long> {
    List<Configuration> findAll();
}
