package com.fitfija.quest.backend.repository;

import com.fitfija.quest.backend.model.domain.News;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface NewsRepository extends CrudRepository<News, Long> {
    List<News> findAll();
}
