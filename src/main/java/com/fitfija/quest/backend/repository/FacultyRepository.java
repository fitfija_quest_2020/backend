package com.fitfija.quest.backend.repository;

import com.fitfija.quest.backend.model.domain.Faculty;
import org.springframework.data.repository.CrudRepository;

public interface FacultyRepository extends CrudRepository<Faculty, Long> {
    Faculty findByName(String name);
}
