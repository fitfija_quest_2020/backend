package com.fitfija.quest.backend.repository;

import com.fitfija.quest.backend.model.domain.Notification;
import com.fitfija.quest.backend.model.domain.NotificationStatus;
import com.fitfija.quest.backend.model.domain.User;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface NotificationRepository extends CrudRepository<Notification, Long> {
    List<Notification> findAllByReceiverAndStatus(User receiver, NotificationStatus status);
}
