package com.fitfija.quest.backend.repository;

import com.fitfija.quest.backend.model.domain.Purchase;
import com.fitfija.quest.backend.model.domain.User;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface PurchaseRepository extends CrudRepository<Purchase, Long> {
    public List<Purchase> findAllByBuyer(User buyer);
}
