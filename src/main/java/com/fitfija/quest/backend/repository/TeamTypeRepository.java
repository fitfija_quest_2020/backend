package com.fitfija.quest.backend.repository;

import com.fitfija.quest.backend.model.domain.TeamType;
import org.springframework.data.repository.CrudRepository;

import java.util.Optional;

public interface TeamTypeRepository extends CrudRepository<TeamType, Long> {
    Optional<TeamType> findByName(String name);
}
