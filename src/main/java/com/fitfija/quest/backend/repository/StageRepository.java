package com.fitfija.quest.backend.repository;

import com.fitfija.quest.backend.model.domain.Stage;
import org.springframework.data.repository.CrudRepository;

public interface StageRepository extends CrudRepository<Stage, Long> {
}
