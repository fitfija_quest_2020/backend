package com.fitfija.quest.backend.repository;

import com.fitfija.quest.backend.model.domain.Item;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface ItemRepository extends CrudRepository<Item, Long> {
    public List<Item> findAll();
}
