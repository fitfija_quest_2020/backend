package com.fitfija.quest.backend.repository;

import com.fitfija.quest.backend.model.domain.Role;
import org.springframework.data.repository.CrudRepository;

public interface RoleRepository extends CrudRepository<Role, Long> {
    Role findByName(String name);
}
