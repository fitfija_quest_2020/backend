package com.fitfija.quest.backend.repository;

import com.fitfija.quest.backend.model.domain.Task;
import com.fitfija.quest.backend.model.domain.Team;
import com.fitfija.quest.backend.model.domain.TeamTaskTry;
import org.springframework.data.repository.CrudRepository;

import java.util.Optional;

public interface TeamTaskTryRepository extends CrudRepository<TeamTaskTry, Long> {
    Optional<TeamTaskTry> findByTeamAndTask(Team team, Task task);
}
