package com.fitfija.quest.backend.repository;

import com.fitfija.quest.backend.model.domain.Team;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface TeamRepository extends CrudRepository<Team, Long> {
    List<Team> findAllByIsHiddenFalseAndTotalPointsGreaterThanOrderByTotalPointsDesc(Integer totalPoints);
}
