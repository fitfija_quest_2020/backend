package com.fitfija.quest.backend.repository;

import com.fitfija.quest.backend.model.domain.User;
import com.fitfija.quest.backend.model.domain.UserStatus;
import org.springframework.data.repository.CrudRepository;

import java.util.List;
import java.util.Optional;

public interface UserRepository extends CrudRepository<User, Long> {
    Optional<User> findByEmail(String email);

    Optional<User> findByInvitationId(Long invitationId);

    List<User> findAllByIsHiddenFalseAndPointsGreaterThanOrderByPointsDesc(Integer points);

    List<User> findAllByIsHiddenFalseAndTeamIsNullAndStatus(UserStatus status);
}
