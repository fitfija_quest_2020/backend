package com.fitfija.quest.backend.repository;

import com.fitfija.quest.backend.model.domain.Answer;
import com.fitfija.quest.backend.model.domain.Task;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface AnswerRepository extends CrudRepository<Answer, Long> {
    List<Answer> findAllByTask(Task task);
}
