package com.fitfija.quest.backend.repository;

import com.fitfija.quest.backend.model.domain.AllowedEmail;
import org.springframework.data.repository.CrudRepository;

import java.util.Optional;

public interface AllowedEmailRepository extends CrudRepository<AllowedEmail, Long> {
    Optional<AllowedEmail> findByEmail(String email);
}
