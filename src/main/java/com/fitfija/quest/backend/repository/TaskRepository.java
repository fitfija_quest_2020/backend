package com.fitfija.quest.backend.repository;

import com.fitfija.quest.backend.model.domain.Stage;
import com.fitfija.quest.backend.model.domain.Task;
import org.springframework.data.repository.CrudRepository;

import java.util.List;
import java.util.Optional;

public interface TaskRepository extends CrudRepository<Task, Long> {
    List<Task> findAllByStage(Stage stage);

    Optional<Task> getById(Long id);
}
