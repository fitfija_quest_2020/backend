package com.fitfija.quest.backend.repository;

import com.fitfija.quest.backend.model.domain.Invitation;
import com.fitfija.quest.backend.model.domain.InvitationStatus;
import com.fitfija.quest.backend.model.domain.User;
import org.springframework.data.repository.CrudRepository;

import java.util.List;
import java.util.Optional;

public interface InvitationRepository extends CrudRepository<Invitation, Long> {
    List<Invitation> findAllBySender(User sender);

    List<Invitation> findAllBySenderAndStatus(User sender, InvitationStatus status);

    List<Invitation> findAllByReceiverAndStatus(User receiver, InvitationStatus status);

    List<Invitation> findAllByStatus(InvitationStatus status);

    Optional<Invitation> findBySenderAndReceiver(User sender, User receiver);

    Optional<Invitation> findBySenderAndStatus(User sender, InvitationStatus status);

    Optional<Invitation> findByReceiverAndStatus(User receiver, InvitationStatus status);
}
