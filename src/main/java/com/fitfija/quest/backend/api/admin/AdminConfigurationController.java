package com.fitfija.quest.backend.api.admin;

import com.fitfija.quest.backend.model.domain.Configuration;
import com.fitfija.quest.backend.model.dto.ResponseMessage;
import com.fitfija.quest.backend.services.ConfigurationService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("/api/admin/config")
@RequiredArgsConstructor
public class AdminConfigurationController {

    private final ConfigurationService configurationService;

    @GetMapping("/get")
    @Operation(summary = "Get current configuration",
            responses = {
                    @ApiResponse(description = "Configuration found successfully", responseCode = "200"),
                    @ApiResponse(description = "Current user does not have administrator role", responseCode = "403"),
                    @ApiResponse(description = "An error occurred", responseCode = "500")
            })
    public ResponseEntity<Configuration> get() {
        return ResponseEntity.ok(configurationService.getConfiguration());
    }

    @PostMapping("/update")
    @Operation(summary = "Update configuration",
            responses = {
                    @ApiResponse(description = "Configuration updated successfully", responseCode = "200"),
                    @ApiResponse(description = "Current user does not have administrator role", responseCode = "403"),
                    @ApiResponse(description = "An error occurred", responseCode = "500")
            })
    public ResponseEntity<ResponseMessage> update(@Valid @RequestBody Configuration configuration) {
        return ResponseEntity.ok(configurationService.updateConfiguration(configuration));
    }
}
