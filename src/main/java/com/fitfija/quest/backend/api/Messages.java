package com.fitfija.quest.backend.api;

public class Messages {
    public static final String LOGIN_ERROR = "Данные для аутентификации содержат ошибку.";
    public static final String USER_NOT_ACTIVE = "Учётная запись заблокирована.";
    public static final String TOKEN_INVALID = "Токен аутентификации недействителен. Нужно заново выполнить вход в аккаунт.";

    //использовать только при попытке сменить пароль!
    public static final String WRONG_PASSWORD = "Неверный пароль.";

    //такое может произойти только если для вошедшего пользователя была удалена запись в бд
    public static final String USER_DELETED = "Ваша учётная запись пропала из базы данных..." ;

    public static final String SUCCESS = "Успешно выполнено.";

    public static final String REGISTRATION_SUCCESS = "Пациент успешно зарегистрирован.";
    public static final String EMAIL_NOT_UNIQUE = "Данная электронная почта уже используется.";
    public static final String EMAIL_NOT_ALLOWED = "Регистрация для указанной электронной почты недоступна.";

    public final static String STAGE_NOT_FOUND = "Этап с таким номером не найден.";
    public static final String STAGE_ENDED = "Этап завершён. Задания недоступны.";
    public static final String STAGE_NOT_STARTED = "Этап ещё не начался. Задания недоступны.";

    public static final String TASK_NOT_FOUND = "Задание c таким номером не найдено.";
    public static final String TASK_IS_FOR_DIFFERENT_FACULTY = "Задание предназначено другому факультету.";
    public static final String TASK_IS_FOR_DIFFERENT_TEAM_TYPE = "Задание предназначено другому типу команд.";
    public static final String TASK_ALREADY_COMPLETED = "Это задание уже выполнено.";
    public static final String TASK_ANSWERING_DISABLED = "На сервере отключена возможность ответить на задание.";

    public static final String RESOURCE_NOT_FOUND = "Запрашиваемый ресурс не был найден.";

    public static final String USER_NOT_FOUND_BY_INVITATION_ID = "Пациент с таким номером карты не найден.";
    public static final String CANNOT_SEND_INVITATION_TO_SAME_FACULTY = "Приглашение можно отправить только пациенту с другого факультета.";
    public static final String INVITATION_LIMIT_REACHED = "Превышен лимит исходящих приглашений.";
    public static final String ALREADY_SENT_INVITATION = "Этому пациенту уже было отправлено приглашение.";
    public static final String ALREADY_RECEIVED_INVITATION = "От этого пациента уже было получено приглашение.";
    public static final String CANNOT_SEND_INVITATION_TO_YOURSELF = "Нельзя отправить приглашение самому себе.";
    public static final String INVITATION_ALREADY_ACCEPTED = "У вас уже есть принятое приглашение.";
    public static final String RECEIVER_ALREADY_HAS_TEAM = "Этот пациент уже имеет команду.";

    public static final String CANNOT_REJECT_INVITATION = "Отклонить можно только входящее приглашение со статусом \"В ожиднии ответа\"";
    public static final String CANNOT_ACCEPT_INVITATION = "Принять можно только входящее приглашение со статусом \"В ожиднии ответа\"";

    public static final String USER_HAS_NO_TEAM = "У пациента нет команды, но для этого действия она должна быть!";
    public static final String TEAM_HAS_MORE_THAN_TWO_PLAYERS = "В команде больше двух участников!";
    public static final String TEAM_HAS_ONLY_ONE_PLAYER = "В команде всего один участник!";

    public static final String NOTIFICATION_AS_SEEN_FORBIDDEN = "Уведомление предназначено другому пациенту, его нельзя отметить как прочитанное.";

    public static final String NOT_ENOUGH_POINTS = "У вас недостаточно очков для покупки!";
    public static final String NO_ITEMS_IN_STOCK = "Этот товар закончился.";

    public static final String MULTIPLE_CONFIGURATIONS = "Ошибка загрузки конфигурации: в базе найдено больше одной записи.";

    public static final String TEAM_TYPE_NOT_FOUND = "Не найден указнный тип команды.";

    public static final String NOT_IMPLEMENTED = "Этот функционал ещё не реализован.";
    public static final String NOT_ENABLED = "Этот функционал был отключён на сервере.";

    public static final String CANNOT_EDIT_PROFILE = "Ха-ха-ха! Этот профиль нельзя редактировать!";

    public static final String ANSWER_CHECKER_NOT_FOUND = "Не найден бин проверки ответа для этого задания.";
}
