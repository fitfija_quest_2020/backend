package com.fitfija.quest.backend.api;

import com.fitfija.quest.backend.model.dto.*;
import com.fitfija.quest.backend.services.TaskService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/tasks")
@RequiredArgsConstructor
public class TaskController {
    private final TaskService taskService;

    @GetMapping("/{id}")
    @Operation(summary = "Get task by id",
            responses = {
                    @ApiResponse(description = "Task found successfully", responseCode = "200"),
                    @ApiResponse(description = "User cannot access this task", responseCode = "403"),
                    @ApiResponse(description = "Task with such id is not found", responseCode = "404"),
            })
    public ResponseEntity<TaskDto> getTask(@PathVariable Long id) {
        TaskDto task = taskService.find(id);

        return ResponseEntity.ok(task);
    }

    @PostMapping("/{id}/answer")
    @Operation(summary = "Answer a task",
            responses = {
                    @ApiResponse(description = "Successfully tried to answer task", responseCode = "200"),
                    @ApiResponse(description = "User cannot access this task", responseCode = "403"),
                    @ApiResponse(description = "Task with such id is not found", responseCode = "404"),
            })
    public ResponseEntity<AnswerTryResponseDto> answerTask(@PathVariable Long id, @RequestBody AnswerTryDto answerTry) {
        return ResponseEntity.ok(taskService.complete(id, answerTry));
    }

    @GetMapping("/canAnswer")
    @Operation(summary = "Check if answering is enabled",
            responses = {
                    @ApiResponse(description = "Check performed successfully", responseCode = "200")
            })
    public ResponseEntity<BooleanResultDto> checkTeamsEnabled() {
        return ResponseEntity.ok(taskService.checkIsAnsweringEnabled());
    }
}
