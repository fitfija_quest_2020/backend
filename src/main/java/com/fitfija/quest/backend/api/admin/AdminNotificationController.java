package com.fitfija.quest.backend.api.admin;

import com.fitfija.quest.backend.model.dto.NotificationToAllDto;
import com.fitfija.quest.backend.model.dto.ResponseMessage;
import com.fitfija.quest.backend.services.NotificationService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/admin/notifications")
@RequiredArgsConstructor
public class AdminNotificationController {

    private final NotificationService notificationService;

    @PostMapping("/sendAll")
    @Operation(summary = "Send a notification to every user",
            responses = {
                    @ApiResponse(description = "Notification sent successfully", responseCode = "200"),
                    @ApiResponse(description = "Current user does not have administrator role", responseCode = "403"),
            })
    public ResponseEntity<ResponseMessage> sendAll(@RequestBody NotificationToAllDto notificationToAllDto) {
        return ResponseEntity.ok(notificationService.sendAll(notificationToAllDto));
    }
}
