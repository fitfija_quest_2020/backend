package com.fitfija.quest.backend.api;

import com.fitfija.quest.backend.model.dto.BooleanResultDto;
import com.fitfija.quest.backend.model.dto.ResponseMessage;
import com.fitfija.quest.backend.model.dto.TeamDto;
import com.fitfija.quest.backend.model.dto.UpdateTeamDto;
import com.fitfija.quest.backend.services.TeamService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("/api/teams")
@RequiredArgsConstructor
public class TeamController {

    private final TeamService teamService;

    @GetMapping("/my")
    @Operation(summary = "Get current user's team",
            responses = {
                    @ApiResponse(description = "Team is found successfully", responseCode = "200"),
                    @ApiResponse(description = "Profile or team is not found in database", responseCode = "404"),
                    @ApiResponse(description = "Teams are disabled on server", responseCode = "400"),
                    @ApiResponse(description = "User has no team", responseCode = "500")
            })
    public ResponseEntity<TeamDto> getMyTeam() {
        return ResponseEntity.ok(teamService.findCurrentUserTeam());
    }

    @PostMapping("/my/edit")
    @Operation(summary = "Update current user's team",
            responses = {
                    @ApiResponse(description = "Team updated successfully", responseCode = "200"),
                    @ApiResponse(description = "Profile or team is not found in database", responseCode = "404"),
                    @ApiResponse(description = "Some fields in request body are not valid OR teams are disabled on server", responseCode = "400"),
                    @ApiResponse(description = "User has no team", responseCode = "500")
            })
    public ResponseEntity<ResponseMessage> updateTeam(@Valid @RequestBody UpdateTeamDto updateTeamDto) {
        return ResponseEntity.ok(teamService.updateCurrentUserTeam(updateTeamDto));
    }

    @GetMapping("/isEnabled")
    @Operation(summary = "Check if teams are enabled",
            responses = {
                    @ApiResponse(description = "Check performed successfully", responseCode = "200")
            })
    public ResponseEntity<BooleanResultDto> checkTeamsEnabled() {
        return ResponseEntity.ok(teamService.checkIsEnabled());
    }
}
