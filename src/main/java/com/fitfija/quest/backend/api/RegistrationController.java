package com.fitfija.quest.backend.api;

import com.fitfija.quest.backend.model.dto.BooleanResultDto;
import com.fitfija.quest.backend.model.dto.RegistrationRequestDto;
import com.fitfija.quest.backend.model.dto.ResponseMessage;
import com.fitfija.quest.backend.services.UserService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("/api")
@RequiredArgsConstructor
public class RegistrationController {
    private final UserService userService;

    @PostMapping("/register")
    @Operation(summary = "Create new user",
            responses = {
                    @ApiResponse(description = "User registered successfully", responseCode = "201"),
                    @ApiResponse(description = "Some fields in request body are empty or null", responseCode = "400"),
                    @ApiResponse(description = "Email is already in use", responseCode = "400"),
                    @ApiResponse(description = "Email is not in allowed list", responseCode = "403")
            })
    public ResponseEntity<ResponseMessage> register(@Valid @RequestBody RegistrationRequestDto registrationRequestDto) {
        userService.registerUser(registrationRequestDto);
        return ResponseEntity.status(HttpStatus.CREATED).body(new ResponseMessage(Messages.REGISTRATION_SUCCESS));

    }

    @PostMapping("/register/isEmailUnique/{email}")
    @Operation(summary = "Check if email is not already taken",
            responses = {@ApiResponse(description = "Check result", responseCode = "200")})
    public ResponseEntity<BooleanResultDto> checkIfEmailIsUnique(@PathVariable String email) {
        return ResponseEntity.ok(new BooleanResultDto(userService.isEmailUnique(email)));
    }

    @PostMapping("/register/isEmailAllowed/{email}")
    @Operation(summary = "Check if email is in allowed list",
            responses = {
                    @ApiResponse(description = "Check result", responseCode = "200")
            })
    public ResponseEntity<BooleanResultDto> checkIfEmailIsAllowed(@PathVariable String email) {
        return ResponseEntity.ok(new BooleanResultDto(userService.isEmailAllowed(email)));
    }

    @GetMapping("/register/isEnabled")
    @Operation(summary = "Check if registration is enabled",
            responses = {
                    @ApiResponse(description = "Check performed successfully", responseCode = "200")
            })
    public ResponseEntity<BooleanResultDto> checkRegistrationEnabled() {
        return ResponseEntity.ok(userService.checkIsRegistrationEnabled());
    }
}
