package com.fitfija.quest.backend.api;

import com.fitfija.quest.backend.model.dto.NotificationDto;
import com.fitfija.quest.backend.model.dto.ResponseMessage;
import com.fitfija.quest.backend.services.NotificationService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/notifications")
@RequiredArgsConstructor
public class NotificationController {
    private final NotificationService notificationService;

    @GetMapping("/new")
    @Operation(summary = "Get a list of new notifications",
            responses = {
                    @ApiResponse(description = "Success", responseCode = "200")
            })
    public ResponseEntity<List<NotificationDto>> findAllNewForCurrentUser() {
        return ResponseEntity.ok(notificationService.findAllNewForCurrentUser());
    }

    @PostMapping("/seen/{id}")
    @Operation(summary = "Mark a notification as seen",
            responses = {
                    @ApiResponse(description = "Success", responseCode = "200"),
                    @ApiResponse(description = "Notification does not belong to current user", responseCode = "403"),
                    @ApiResponse(description = "Notification not found", responseCode = "404")
            })
    public ResponseEntity<ResponseMessage> markAsSeen(@PathVariable Long id) {
        return ResponseEntity.ok(notificationService.markAsSeen(id));
    }
}
