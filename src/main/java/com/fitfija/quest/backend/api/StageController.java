package com.fitfija.quest.backend.api;

import com.fitfija.quest.backend.model.dto.StageDto;
import com.fitfija.quest.backend.services.StageService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/stage")
@RequiredArgsConstructor
public class StageController {

    private final StageService stageService;

    @GetMapping("/{id}")
    @Operation(summary = "Get stage by id",
            responses = {
                    @ApiResponse(description = "Stage found successfully", responseCode = "200"),
                    @ApiResponse(description = "User cannot access this stage because it hasn't started or has already ended",
                            responseCode = "403"),
                    @ApiResponse(description = "Stage with such id is not found", responseCode = "404"),
            })
    public ResponseEntity<StageDto> getStage(@PathVariable Long id) {
        StageDto stage = stageService.find(id);
        return ResponseEntity.ok(stage);
    }

    @GetMapping("/current")
    @Operation(summary = "Get current stage, as set on server",
            responses = {
                    @ApiResponse(description = "Stage found successfully", responseCode = "200"),
                    @ApiResponse(description = "User cannot access current stage because it hasn't started or has already ended",
                            responseCode = "403"),
                    @ApiResponse(description = "Current stage is not found or not set", responseCode = "404"),
            })
    public ResponseEntity<StageDto> getCurrentStage() {
        StageDto stage = stageService.findCurrent();
        return ResponseEntity.ok(stage);
    }
}
