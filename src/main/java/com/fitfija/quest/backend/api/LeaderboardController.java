package com.fitfija.quest.backend.api;

import com.fitfija.quest.backend.model.dto.BooleanResultDto;
import com.fitfija.quest.backend.model.dto.LeaderboardEntryDto;
import com.fitfija.quest.backend.services.LeaderboardService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/api/leaderboard")
@RequiredArgsConstructor
public class LeaderboardController {

    private final LeaderboardService leaderboardService;

    @GetMapping("/get")
    @Operation(summary = "Get leaderboard",
            responses = {
                    @ApiResponse(description = "Success", responseCode = "200"),
                    @ApiResponse(description = "Leaderboard is disabled", responseCode = "500")
            })
    public ResponseEntity<List<LeaderboardEntryDto>> getLeaderboard() {
        return ResponseEntity.ok(leaderboardService.getLeaderboard());
    }

    @GetMapping("/isEnabled")
    @Operation(summary = "Check if leaderboard is enabled",
            responses = {
                    @ApiResponse(description = "Check performed successfully", responseCode = "200")
            })
    public ResponseEntity<BooleanResultDto> checkLeaderboardEnabled() {
        return ResponseEntity.ok(leaderboardService.checkIsEnabled());
    }
}
