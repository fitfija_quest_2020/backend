package com.fitfija.quest.backend.api.admin;

import com.fitfija.quest.backend.model.dto.TeamDto;
import com.fitfija.quest.backend.services.TeamService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/api/admin/teams")
@RequiredArgsConstructor
public class AdminTeamController {

    private final TeamService teamService;

    @PostMapping("/generateFromInvitations")
    public ResponseEntity<List<TeamDto>> generateFromInvitation() {
        return ResponseEntity.ok(teamService.generateFromInvitations());
    }

    @PostMapping("/generateRandom")
    public ResponseEntity<List<TeamDto>> generateRandom() {
        return ResponseEntity.ok(teamService.generateRandom());
    }
}
