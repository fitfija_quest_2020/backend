package com.fitfija.quest.backend.api;

import com.fitfija.quest.backend.model.dto.AuthenticationRequestDto;
import com.fitfija.quest.backend.model.dto.AuthenticationResponseDto;
import com.fitfija.quest.backend.services.UserService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@RestController
@RequestMapping("/api")
@RequiredArgsConstructor
public class AuthenticationController {
    private final UserService userService;

    @PostMapping("/login")
    @Operation(summary = "User authentication",
            responses = {
                    @ApiResponse(description = "Default response is the same email " +
                            "and generated token if authentication is successful", responseCode = "200"),
                    @ApiResponse(description = "Email or password is incorrect", responseCode = "401")
            })
    public ResponseEntity<AuthenticationResponseDto> login(
            @Parameter(description = "Request with email and password", required = true)
            @Valid @RequestBody AuthenticationRequestDto requestDto) {
        AuthenticationResponseDto authenticationResponseDto = userService.login(requestDto);
        return ResponseEntity.ok(authenticationResponseDto);
    }
}
