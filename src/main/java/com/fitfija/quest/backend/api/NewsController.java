package com.fitfija.quest.backend.api;

import com.fitfija.quest.backend.model.dto.NewsDto;
import com.fitfija.quest.backend.services.NewsService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/api/news")
@RequiredArgsConstructor
public class NewsController {
    private final NewsService newsService;

    @GetMapping("/all")
    @Operation(summary = "Get all news",
            responses = {
                    @ApiResponse(description = "Found all news successfully", responseCode = "200"),
            })
    public ResponseEntity<List<NewsDto>> getAllNews() {
        return ResponseEntity.ok(newsService.findAll());
    }
}
