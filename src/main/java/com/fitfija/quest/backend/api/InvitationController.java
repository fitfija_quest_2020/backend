package com.fitfija.quest.backend.api;

import com.fitfija.quest.backend.model.dto.*;
import com.fitfija.quest.backend.services.InvitationService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/api/invitations")
@RequiredArgsConstructor
public class InvitationController {
    private final InvitationService invitationService;

    @GetMapping("/inbox")
    @Operation(summary = "Get all incoming invitations, except declined",
            responses = {
                    @ApiResponse(description = "Found all incoming invitations successfully", responseCode = "200"),
                    @ApiResponse(description = "Invitations are disabled on server", responseCode = "400")
            })
    public ResponseEntity<List<InvitationDto>> getInbox() {
        return ResponseEntity.ok(invitationService.findAllInbox());
    }

    @GetMapping("/outbox")
    @Operation(summary = "Get all outgoing invitations",
            responses = {
                    @ApiResponse(description = "Found all outgoing invitations successfully", responseCode = "200"),
                    @ApiResponse(description = "Invitations are disabled on server", responseCode = "400")
            })
    public ResponseEntity<List<InvitationDto>> getOutbox() {
        return ResponseEntity.ok(invitationService.findAllOutbox());
    }

    @PostMapping("/send")
    @Operation(summary = "Send a new invitation",
            responses = {
                    @ApiResponse(description = "Created new invitation successfully", responseCode = "201"),
                    @ApiResponse(description = "User with such invitationId not found", responseCode = "404"),
                    @ApiResponse(description = "Cannot send an invitation, reason in message", responseCode = "400")
            })
    public ResponseEntity<ResponseMessage> sendInvitation(@Valid @RequestBody NewInvitationRequestDto newInvitationRequestDto) {
        invitationService.sendInvitation(newInvitationRequestDto);
        return ResponseEntity.status(HttpStatus.CREATED).body(new ResponseMessage(Messages.SUCCESS));
    }

    @PostMapping("/reject/{id}")
    @Operation(summary = "Reject an invitation",
            responses = {
                    @ApiResponse(description = "Rejected successfully", responseCode = "200"),
                    @ApiResponse(description = "Invitation not found", responseCode = "404"),
                    @ApiResponse(description = "Cannot reject an invitation, reason in message", responseCode = "400")
            })
    public ResponseEntity<ResponseMessage> rejectInvitation(@PathVariable Long id) {
        invitationService.reject(id);
        return ResponseEntity.ok(new ResponseMessage(Messages.SUCCESS));
    }

    @PostMapping("/accept/{id}")
    @Operation(summary = "Accept an invitation",
            responses = {
                    @ApiResponse(description = "Accepted successfully", responseCode = "200"),
                    @ApiResponse(description = "Invitation not found", responseCode = "404"),
                    @ApiResponse(description = "Cannot accept an invitation, reason in message", responseCode = "400")
            })
    public ResponseEntity<ResponseMessage> acceptInvitation(@PathVariable Long id) {
        invitationService.accept(id);
        return ResponseEntity.ok(new ResponseMessage(Messages.SUCCESS));
    }

    @GetMapping("/isEnabled")
    @Operation(summary = "Check if invitations are enabled",
            responses = {
                    @ApiResponse(description = "Check performed successfully", responseCode = "200")
            })
    public ResponseEntity<BooleanResultDto> checkInvitationsEnabled() {
        return ResponseEntity.ok(invitationService.checkIsEnabled());
    }

    @GetMapping("/info")
    @Operation(summary = "Check if current user has accepted invitation",
            responses = {
                    @ApiResponse(description = "Check performed successfully", responseCode = "200"),
                    @ApiResponse(description = "Invitations are disabled on server", responseCode = "400")
            })
    public ResponseEntity<InvitationsInfoDto> checkInvitationsInfo() {
        return ResponseEntity.ok(invitationService.getInvitationsInfo());
    }
}
