package com.fitfija.quest.backend.api;

import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api")
@RequiredArgsConstructor
public class SpecialController {

    @GetMapping("secret")
    public ResponseEntity<String> secret() {
        return ResponseEntity.ok("phoenicopterus roseus");
    }
}
