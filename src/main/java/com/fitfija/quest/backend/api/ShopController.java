package com.fitfija.quest.backend.api;

import com.fitfija.quest.backend.model.dto.BooleanResultDto;
import com.fitfija.quest.backend.model.dto.ItemDto;
import com.fitfija.quest.backend.model.dto.ResponseMessage;
import com.fitfija.quest.backend.services.ShopService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/shop")
@RequiredArgsConstructor
public class ShopController {

    private final ShopService shopService;

    @GetMapping("/all")
    @Operation(summary = "Get a list of all items",
            responses = {
                    @ApiResponse(description = "Success", responseCode = "200"),
                    @ApiResponse(description = "Shop is disabled on server", responseCode = "400")
            })
    public ResponseEntity<List<ItemDto>> findAllItems() {
        return ResponseEntity.ok(shopService.findAllItems());
    }

    @PostMapping("/purchase/{id}")
    @Operation(summary = "Purchase an item",
            responses = {
                    @ApiResponse(description = "Success", responseCode = "200"),
                    @ApiResponse(description = "Item not found", responseCode = "404"),
                    @ApiResponse(description = "Shop is disabled on server OR other error", responseCode = "400")
            })
    public ResponseEntity<ResponseMessage> makePurchase(@PathVariable Long id) {
        return ResponseEntity.ok(shopService.makePurchase(id));
    }

    @GetMapping("/isEnabled")
    @Operation(summary = "Check if shop is enabled",
            responses = {
                    @ApiResponse(description = "Check performed successfully", responseCode = "200")
            })
    public ResponseEntity<BooleanResultDto> checkTeamsEnabled() {
        return ResponseEntity.ok(shopService.checkIsEnabled());
    }
}
