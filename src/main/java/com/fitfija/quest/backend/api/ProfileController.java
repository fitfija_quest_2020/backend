package com.fitfija.quest.backend.api;

import com.fitfija.quest.backend.model.dto.*;
import com.fitfija.quest.backend.services.ShopService;
import com.fitfija.quest.backend.services.UserService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/api/profile")
@RequiredArgsConstructor
public class ProfileController {

    private final UserService userService;
    private final ShopService shopService;

    @GetMapping("/my")
    @Operation(summary = "Get current user profile info",
            responses = {
                    @ApiResponse(description = "Profile is found successfully", responseCode = "200"),
                    @ApiResponse(description = "Profile is not found in database", responseCode = "404")
            })
    public ResponseEntity<UserDto> getMyProfile() {
        return ResponseEntity.ok(userService.findCurrentUser());
    }

    @GetMapping("/teammate")
    @Operation(summary = "Get current user's teammate profile info",
            responses = {
                    @ApiResponse(description = "Profile is found successfully", responseCode = "200"),
                    @ApiResponse(description = "Profile is not found in database", responseCode = "404")
            })
    public ResponseEntity<UserDto> getTeammateProfile() {
        return ResponseEntity.ok(userService.findCurrentUsersTeammate());
    }

    @PostMapping("/my/changePassword")
    @Operation(summary = "Change current user's password",
            responses = {
                    @ApiResponse(description = "Password is changed successfully", responseCode = "200"),
                    @ApiResponse(description = "Profile is not found in database", responseCode = "404"),
                    @ApiResponse(description = "Some fields in request body are empty or null", responseCode = "400"),
                    @ApiResponse(description = "Current password is incorrect", responseCode = "401")
            })
    public ResponseEntity<ResponseMessage> changePassword(@Valid @RequestBody PasswordChangeDto passwordChangeDto) {
        return ResponseEntity.ok(userService.changePassword(passwordChangeDto));
    }

    // я очень долго мучился, но так и не смог заставить cors пропускать put запросы...
    @PostMapping("/my/edit")
    @Operation(summary = "Update current user profile",
            responses = {
                    @ApiResponse(description = "User profile updated successfully", responseCode = "200"),
                    @ApiResponse(description = "Profile is not found in database", responseCode = "404"),
                    @ApiResponse(description = "Some fields in request body are empty or null", responseCode = "400")
            })
    public ResponseEntity<ResponseMessage> updateProfile(@Valid @RequestBody UpdateUserDto updateUserDto) {
        return ResponseEntity.ok(userService.updateUser(updateUserDto));
    }

    @GetMapping("/my/isAdmin")
    @Operation(summary = "Check if current user has admin role",
            responses = {
                    @ApiResponse(description = "Check performed successfully", responseCode = "200"),
                    @ApiResponse(description = "Profile is not found in database", responseCode = "404")
            })
    public ResponseEntity<BooleanResultDto> isCurrentUserAdmin() {
        return ResponseEntity.ok(userService.isCurrentUserAdmin());
    }

    @GetMapping("/my/purchases")
    @Operation(summary = "Get current user's purchases",
            responses = {
                    @ApiResponse(description = "Success", responseCode = "200"),
                    @ApiResponse(description = "Profile is not found in database", responseCode = "404"),
                    @ApiResponse(description = "Shop is not enabled OR other error", responseCode = "400")
            })
    public ResponseEntity<List<PurchaseDto>> findPurchases() {
        return ResponseEntity.ok(shopService.findCurrentUserPurchases());
    }
}
