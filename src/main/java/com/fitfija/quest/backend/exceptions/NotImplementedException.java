package com.fitfija.quest.backend.exceptions;

import com.fitfija.quest.backend.api.Messages;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.NOT_IMPLEMENTED)
public class NotImplementedException extends RuntimeException {
    public NotImplementedException() {
        super(Messages.NOT_IMPLEMENTED);
    }
}
