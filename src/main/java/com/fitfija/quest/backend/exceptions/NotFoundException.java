package com.fitfija.quest.backend.exceptions;

import com.fitfija.quest.backend.api.Messages;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.NOT_FOUND)
public class NotFoundException extends RuntimeException {
    public NotFoundException(String msg, Throwable t) {
        super(msg, t);
    }

    public NotFoundException(String msg) {
        super(msg);
    }

    public NotFoundException() {
        super(Messages.RESOURCE_NOT_FOUND);
    }
}
