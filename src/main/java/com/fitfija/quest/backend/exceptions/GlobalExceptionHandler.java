package com.fitfija.quest.backend.exceptions;

import com.fitfija.quest.backend.api.Messages;
import com.fitfija.quest.backend.model.dto.ResponseMessage;
import org.springframework.beans.factory.NoSuchBeanDefinitionException;
import org.springframework.core.annotation.AnnotationUtils;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.DisabledException;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.server.ResponseStatusException;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@ControllerAdvice
@EnableWebMvc
public class GlobalExceptionHandler extends ResponseEntityExceptionHandler {

    @ExceptionHandler(RuntimeException.class)
    public ResponseEntity<?> handleException(RuntimeException ex, WebRequest request) {
        // кидаем дальше ошибки с аннотацией ResponseStatus или классом ResponseStatusException
        // они и так нормально обработаются

        if (AnnotationUtils.findAnnotation(ex.getClass(), ResponseStatus.class) != null ||
                ex.getClass() == ResponseStatusException.class) {
            throw ex;
        }

        HttpStatus status;
        String message;

        if (ex.getClass() == JwtAuthenticationException.class) {
            status = HttpStatus.UNAUTHORIZED;
            message = ex.getMessage();
        } else if (ex.getClass() == DisabledException.class) {
            status = HttpStatus.FORBIDDEN;
            message = Messages.USER_NOT_ACTIVE;
        } else if (ex.getClass() == NoSuchBeanDefinitionException.class) {
            status = HttpStatus.INTERNAL_SERVER_ERROR;
            message = Messages.ANSWER_CHECKER_NOT_FOUND;
        } else {
            status = HttpStatus.INTERNAL_SERVER_ERROR;
            message = ex.getClass().getName() + ": " + ex.getMessage();
        }

        return handleExceptionInternal(ex, new ResponseMessage(message), new HttpHeaders(), status, request);
    }

    @Override
    public ResponseEntity<Object> handleMethodArgumentNotValid(MethodArgumentNotValidException ex, HttpHeaders headers,
                                                               HttpStatus status, WebRequest request) {
        // здесь нужен отдельный метод, потому что MethodArgumentNotValidException - это не RuntimeException

        StringBuilder stringBuilder = new StringBuilder("Ошибка: ");

        ex.getBindingResult().getAllErrors().forEach((error) -> {
            String fieldName = ((FieldError) error).getField();
            String errorMessage = error.getDefaultMessage();
            stringBuilder.append(fieldName).append(" - ").append(errorMessage).append("; ");
        });

        return handleExceptionInternal(ex, new ResponseMessage(stringBuilder.toString()),
                headers, HttpStatus.BAD_REQUEST, request);
    }
}
