package com.fitfija.quest.backend.exceptions;

import com.fitfija.quest.backend.api.Messages;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.BAD_REQUEST)
public class NotEnabledException extends RuntimeException {
    public NotEnabledException() {
        super(Messages.NOT_ENABLED);
    }
}
