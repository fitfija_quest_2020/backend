package com.fitfija.quest.backend.model.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
public class AnswerTryResponseDto {
    private AnswerTryResult result;

    public AnswerTryResponseDto(boolean result) {
        this.result = result ? AnswerTryResult.CORRECT : AnswerTryResult.INCORRECT;
    }

    enum AnswerTryResult {
        CORRECT, INCORRECT;
    }
}
