package com.fitfija.quest.backend.model.mappers;

import com.fitfija.quest.backend.exceptions.NotFoundException;
import com.fitfija.quest.backend.model.domain.User;
import com.fitfija.quest.backend.model.dto.UserDto;
import com.fitfija.quest.backend.repository.FacultyRepository;
import com.fitfija.quest.backend.repository.TeamRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

@Component
public class UserMapper extends AbstractMapper<User, UserDto> {

    private final FacultyRepository facultyRepository;
    private final TeamRepository teamRepository;

    @Autowired
    UserMapper(FacultyRepository facultyRepository, TeamRepository teamRepository) {
        super(User.class, UserDto.class);
        this.facultyRepository = facultyRepository;
        this.teamRepository = teamRepository;
    }

    @PostConstruct
    public void setupMapper() {
        mapper.createTypeMap(User.class, UserDto.class)
                .addMappings(m -> m.skip(UserDto::setFaculty)).setPostConverter(toDtoConverter())
                .addMappings(m -> m.skip(UserDto::setTeamId)).setPostConverter(toDtoConverter());
        mapper.createTypeMap(UserDto.class, User.class)
                .addMappings(m -> m.skip(User::setFaculty)).setPostConverter(toEntityConverter())
                .addMappings(m -> m.skip(User::setTeam)).setPostConverter(toEntityConverter());
    }

    @Override
    public void mapSpecificFields(User source, UserDto destination) {
        destination.setFaculty(source.getFaculty().getName());
        if (source.getTeam() != null) {
            destination.setTeamId(source.getTeam().getId());
        } else {
            destination.setTeamId(null);
        }
    }

    @Override
    void mapSpecificFields(UserDto source, User destination) {
        destination.setFaculty(facultyRepository.findByName(source.getFaculty()));
        if (source.getTeamId() != null) {
            destination.setTeam(teamRepository.findById(source.getTeamId()).orElseThrow(NotFoundException::new));
        } else {
            destination.setTeam(null);
        }
    }
}
