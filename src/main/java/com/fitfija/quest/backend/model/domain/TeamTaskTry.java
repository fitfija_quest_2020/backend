package com.fitfija.quest.backend.model.domain;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;

@Entity
@Getter
@Setter
@NoArgsConstructor
public class TeamTaskTry extends AuditingEntity {

    public TeamTaskTry(Team team, Task task) {
        this.team = team;
        this.task = task;
        this.tries = 0;
    }

    @ManyToOne
    private Team team;

    @ManyToOne
    private Task task;

    private Integer tries;

    public int increment() {
        return tries++;
    }
}
