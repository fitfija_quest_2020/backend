package com.fitfija.quest.backend.model.domain;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Entity;

@Entity
@Getter
@Setter
public class Item extends AuditingEntity {
    private String name;

    private String description;

    private Integer inStock;

    private Integer price;

    private String imageUrl;
}
