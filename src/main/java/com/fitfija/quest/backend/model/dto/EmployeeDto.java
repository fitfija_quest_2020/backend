package com.fitfija.quest.backend.model.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class EmployeeDto extends AuditingDto{
    private String name;
    private String position;
    private String description;
    private String imageUrl;
}
