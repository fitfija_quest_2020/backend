package com.fitfija.quest.backend.model.mappers;

import com.fitfija.quest.backend.model.domain.Answer;
import com.fitfija.quest.backend.model.dto.AnswerDto;
import com.fitfija.quest.backend.repository.TaskRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

@Component
public class AnswerMapper extends AbstractMapper<Answer, AnswerDto> {
    private final TaskRepository taskRepository;

    @Autowired
    AnswerMapper(TaskRepository taskRepository) {
        super(Answer.class, AnswerDto.class);
        this.taskRepository = taskRepository;
    }

    @PostConstruct
    public void setupMapper() {
        mapper.createTypeMap(Answer.class, AnswerDto.class)
                .addMappings(m -> m.skip(AnswerDto::setTaskId)).setPostConverter(toDtoConverter());
        mapper.createTypeMap(AnswerDto.class, Answer.class)
                .addMappings(m -> m.skip(Answer::setTask)).setPostConverter(toEntityConverter());
    }

    @Override
    public void mapSpecificFields(Answer source, AnswerDto destination) {
        destination.setTaskId(getId(source));
    }

    @Override
    void mapSpecificFields(AnswerDto source, Answer destination) {
        destination.setTask(taskRepository.findById(source.getTaskId()).orElse(null));
    }

    private Long getId(Answer source) {
        return source == null || source.getTask() == null ? null : source.getTask().getId();
    }
}
