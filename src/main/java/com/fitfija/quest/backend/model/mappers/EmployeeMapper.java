package com.fitfija.quest.backend.model.mappers;

import com.fitfija.quest.backend.model.domain.Employee;
import com.fitfija.quest.backend.model.dto.EmployeeDto;
import org.springframework.stereotype.Component;

@Component
public class EmployeeMapper extends AbstractMapper<Employee, EmployeeDto> {
    EmployeeMapper() {
        super(Employee.class, EmployeeDto.class);
    }
}
