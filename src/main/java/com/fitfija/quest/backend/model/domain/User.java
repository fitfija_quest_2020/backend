package com.fitfija.quest.backend.model.domain;

import lombok.*;

import javax.persistence.*;
import java.util.List;
import java.util.Set;

@Entity
@Table(name = "users")              //потому что user занята постгресом
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
public class User extends AuditingEntity {

    public User(String email, String password, String firstName, String lastName, String contactInfo,
                Long invitationId, Faculty faculty, Boolean isInAcadem, List<Role> roles) {
        this.email = email;
        this.password = password;
        this.firstName = firstName;
        this.lastName = lastName;
        this.contactInfo = contactInfo;
        this.invitationId = invitationId;
        this.faculty = faculty;
        this.isInAcadem = isInAcadem;
        this.roles = roles;

        this.status = UserStatus.ACTIVE;
        this.points = 0;
        this.isHidden = false;
    }

    private String email;           //почта используется как логин
    private String password;

    @Enumerated(EnumType.STRING)
    private UserStatus status;

    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(name = "user_role",
            joinColumns = {@JoinColumn(name = "user_id", referencedColumnName = "id")},
            inverseJoinColumns = {@JoinColumn(name = "role_id", referencedColumnName = "id")})
    private List<Role> roles;

    private String firstName;

    private String lastName;

    private String contactInfo;

    private Long invitationId;

    private Integer points;

    @ManyToOne
    private Faculty faculty;

    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(name = "user_task",
            joinColumns = {@JoinColumn(name = "user_id", referencedColumnName = "id")},
            inverseJoinColumns = {@JoinColumn(name = "task_id", referencedColumnName = "id")})
    private Set<Task> completedTasks;

    @ManyToOne
    private Team team;

    private Boolean isHidden;

    private Boolean isInAcadem;

    @Enumerated(EnumType.STRING)
    private Gender gender;

    public boolean isAdmin() {
        return roles.stream().anyMatch(r -> r.getName().equals("ROLE_ADMIN"));
    }
}
