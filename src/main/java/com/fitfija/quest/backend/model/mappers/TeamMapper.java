package com.fitfija.quest.backend.model.mappers;

import com.fitfija.quest.backend.exceptions.NotFoundException;
import com.fitfija.quest.backend.model.domain.Team;
import com.fitfija.quest.backend.model.dto.TeamDto;
import com.fitfija.quest.backend.repository.TeamTypeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

@Component
public class TeamMapper extends AbstractMapper<Team, TeamDto> {
    private final TeamTypeRepository teamTypeRepository;

    @Autowired
    TeamMapper(TeamTypeRepository teamTypeRepository) {
        super(Team.class, TeamDto.class);
        this.teamTypeRepository = teamTypeRepository;
    }

    @PostConstruct
    public void setupMapper() {
        mapper.createTypeMap(Team.class, TeamDto.class)
                .addMappings(m -> m.skip(TeamDto::setType)).setPostConverter(toDtoConverter());
        mapper.createTypeMap(TeamDto.class, Team.class)
                .addMappings(m -> m.skip(Team::setType)).setPostConverter(toEntityConverter());
    }

    @Override
    public void mapSpecificFields(Team source, TeamDto destination) {
        destination.setType(source.getType().getName());
    }

    @Override
    void mapSpecificFields(TeamDto source, Team destination) {
        destination.setType(teamTypeRepository.findByName(source.getType()).orElseThrow(NotFoundException::new));
    }
}
