package com.fitfija.quest.backend.model.domain;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.List;

@Entity
@Getter
@Setter
@EqualsAndHashCode(callSuper = true)
public class Task extends AuditingEntity {

    @ManyToOne
    private Stage stage;

    private String name;

    private String text;

    private String imageUrl;

    private int points;

    private boolean isHidden;

    @OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL, mappedBy = "task")
    private List<Answer> answers;

    @ManyToOne
    private Faculty faculty;

    @ManyToOne
    private TeamType teamType;

    private String answerCheckerBean;
}
