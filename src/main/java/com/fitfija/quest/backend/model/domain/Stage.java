package com.fitfija.quest.backend.model.domain;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.List;

@Entity
@Getter
@Setter
public class Stage extends AuditingEntity {

    private String name;

    private LocalDateTime start;

    private LocalDateTime end;

    @OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL, mappedBy = "stage")
    private List<Task> tasks;

    public boolean hasStarted() {
        return start == null || start.isBefore(LocalDateTime.now());
    }

    public boolean hasEnded() {
        return end != null && end.isBefore(LocalDateTime.now());
    }
}
