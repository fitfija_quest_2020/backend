package com.fitfija.quest.backend.model.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class TeamDto extends AuditingDto {
    private String name;

    private List<UserDto> players;

    private Integer currentPoints;

    private Integer totalPoints;

    private String type;
}
