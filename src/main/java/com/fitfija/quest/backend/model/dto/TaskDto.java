package com.fitfija.quest.backend.model.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class TaskDto extends AuditingDto {
    private Long stageId;

    private Boolean isHidden;

    private Boolean isCompleted;

    private Integer tries;

    private String name;

    private String text;

    private String imageUrl;

    private Integer points;
}
