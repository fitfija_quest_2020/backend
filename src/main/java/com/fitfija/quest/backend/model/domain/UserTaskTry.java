package com.fitfija.quest.backend.model.domain;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;

@Entity
@Getter
@Setter
@NoArgsConstructor
public class UserTaskTry extends AuditingEntity {

    public UserTaskTry(User user, Task task) {
        this.user = user;
        this.task = task;
        this.tries = 0;
    }

    @ManyToOne
    private User user;

    @ManyToOne
    private Task task;

    private Integer tries;

    public int increment() {
        return tries++;
    }
}
