package com.fitfija.quest.backend.model.domain;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Entity;

@Getter
@Setter
@Entity
public class News extends AuditingEntity {
    private String headline;
    private String text;
}
