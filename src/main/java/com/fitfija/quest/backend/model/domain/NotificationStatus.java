package com.fitfija.quest.backend.model.domain;

public enum NotificationStatus {
    NEW, SEEN
}
