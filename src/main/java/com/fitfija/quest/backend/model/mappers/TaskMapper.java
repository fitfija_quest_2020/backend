package com.fitfija.quest.backend.model.mappers;

import com.fitfija.quest.backend.api.Messages;
import com.fitfija.quest.backend.model.domain.*;
import com.fitfija.quest.backend.model.dto.TaskDto;
import com.fitfija.quest.backend.repository.StageRepository;
import com.fitfija.quest.backend.repository.TeamTaskTryRepository;
import com.fitfija.quest.backend.repository.UserTaskTryRepository;
import com.fitfija.quest.backend.security.UserManager;
import com.fitfija.quest.backend.services.ConfigurationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import org.springframework.web.server.ResponseStatusException;

import javax.annotation.PostConstruct;

@Component
public class TaskMapper extends AbstractMapper<Task, TaskDto> {
    private final StageRepository stageRepository;

    private final UserTaskTryRepository userTaskTryRepository;
    private final TeamTaskTryRepository teamTaskTryRepository;

    private final ConfigurationService configurationService;

    private final UserManager userManager;

    @Autowired
    TaskMapper(StageRepository stageRepository, UserTaskTryRepository userTaskTryRepository,
               TeamTaskTryRepository teamTaskTryRepository, ConfigurationService configurationService,
               UserManager userManager) {
        super(Task.class, TaskDto.class);
        this.stageRepository = stageRepository;
        this.userTaskTryRepository = userTaskTryRepository;
        this.teamTaskTryRepository = teamTaskTryRepository;
        this.configurationService = configurationService;
        this.userManager = userManager;
    }

    @PostConstruct
    public void setupMapper() {
        mapper.createTypeMap(Task.class, TaskDto.class)
                .addMappings(m -> m.skip(TaskDto::setStageId)).setPostConverter(toDtoConverter())
                .addMappings(m -> m.skip(TaskDto::setIsCompleted)).setPostConverter(toDtoConverter())
                .addMappings(m -> m.skip(TaskDto::setTries)).setPostConverter(toDtoConverter());
        mapper.createTypeMap(TaskDto.class, Task.class)
                .addMappings(m -> m.skip(Task::setStage)).setPostConverter(toEntityConverter());
    }

    @Override
    public void mapSpecificFields(Task source, TaskDto destination) {
        destination.setStageId(getId(source));

        User user = userManager.getCurrentUser();

        if (configurationService.getConfiguration().getTeamsEnabled()) {
            Team team = user.getTeam();

            if (team == null) {
                throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, Messages.USER_HAS_NO_TEAM);
            }

            TeamTaskTry teamTaskTry = teamTaskTryRepository.findByTeamAndTask(team, source)
                    .orElseGet(() -> new TeamTaskTry(team, source));

            destination.setTries(teamTaskTry.getTries());
            destination.setIsCompleted(team.getCompletedTasks().contains(source));
        } else {
            UserTaskTry userTaskTry = userTaskTryRepository.findByUserAndTask(user, source)
                    .orElseGet(() -> new UserTaskTry(user, source));

            destination.setTries(userTaskTry.getTries());
            destination.setIsCompleted(user.getCompletedTasks().contains(source));
        }
    }

    @Override
    void mapSpecificFields(TaskDto source, Task destination) {
        destination.setStage(stageRepository.findById(source.getStageId()).orElse(null));
    }

    private Long getId(Task source) {
        return source == null || source.getStage() == null ? null : source.getStage().getId();
    }
}
