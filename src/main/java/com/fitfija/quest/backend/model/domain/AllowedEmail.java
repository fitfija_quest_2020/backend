package com.fitfija.quest.backend.model.domain;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;

@Entity
@Getter
@Setter
public class AllowedEmail extends AbstractEntity {
    private String email;

    @ManyToOne
    private Faculty faculty;
}
