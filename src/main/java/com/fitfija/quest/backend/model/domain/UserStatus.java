package com.fitfija.quest.backend.model.domain;

public enum UserStatus {
    ACTIVE, NOT_ACTIVE
}
