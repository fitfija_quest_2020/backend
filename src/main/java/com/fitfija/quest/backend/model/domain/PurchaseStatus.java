package com.fitfija.quest.backend.model.domain;

public enum PurchaseStatus {
    IN_TRANSIT, DELIVERED, CANCELED
}
