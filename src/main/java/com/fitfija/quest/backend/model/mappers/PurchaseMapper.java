package com.fitfija.quest.backend.model.mappers;

import com.fitfija.quest.backend.model.domain.Purchase;
import com.fitfija.quest.backend.model.dto.PurchaseDto;
import org.springframework.stereotype.Component;

@Component
public class PurchaseMapper extends AbstractMapper<Purchase, PurchaseDto> {
    PurchaseMapper() {
        super(Purchase.class, PurchaseDto.class);
    }
}
