package com.fitfija.quest.backend.model.mappers;

import com.fitfija.quest.backend.model.domain.Notification;
import com.fitfija.quest.backend.model.dto.NotificationDto;
import org.springframework.stereotype.Component;

@Component
public class NotificationMapper extends AbstractMapper<Notification, NotificationDto> {
    NotificationMapper() {
        super(Notification.class, NotificationDto.class);
    }
}
