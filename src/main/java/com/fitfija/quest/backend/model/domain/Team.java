package com.fitfija.quest.backend.model.domain;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.Set;

@Entity
@Getter
@Setter
@NoArgsConstructor
public class Team extends AuditingEntity {

    public Team(String name, TeamType teamType) {
        this.name = name;
        this.type = teamType;

        this.isHidden = false;
        this.currentPoints = 0;
        this.totalPoints = 0;
    }

    private String name;

    private Boolean isHidden;

    @OneToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL, mappedBy = "team")
    private Set<User> players;

    private Integer currentPoints;

    private Integer totalPoints;

    @ManyToOne
    private TeamType type;

    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(name = "team_task",
            joinColumns = {@JoinColumn(name = "team_id", referencedColumnName = "id")},
            inverseJoinColumns = {@JoinColumn(name = "task_id", referencedColumnName = "id")})
    private Set<Task> completedTasks;

    public void addPoints(int points) {
        currentPoints += points;
        totalPoints += points;
    }

    public void subtractPoints(int points) {
        if (currentPoints - points < 0) {
            throw new IllegalArgumentException("Cannot make currentPoints negative");
        }

        currentPoints -= points;
    }
}
