package com.fitfija.quest.backend.model.mappers;

import com.fitfija.quest.backend.model.domain.Faculty;
import com.fitfija.quest.backend.model.dto.FacultyDto;
import org.springframework.stereotype.Component;

@Component
public class FacultyMapper extends AbstractMapper<Faculty, FacultyDto> {
    FacultyMapper() {
        super(Faculty.class, FacultyDto.class);
    }
}
