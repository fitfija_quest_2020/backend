package com.fitfija.quest.backend.model.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.Size;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class AuthenticationRequestDto {
    @Size(max = 255, min = 4)
    private String email;
    @Size(max = 255, min = 6)
    private String password;
}
