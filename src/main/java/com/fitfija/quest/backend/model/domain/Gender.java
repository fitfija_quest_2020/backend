package com.fitfija.quest.backend.model.domain;

public enum Gender {
    FEMALE, MALE, OTHER, UNKNOWN
}
