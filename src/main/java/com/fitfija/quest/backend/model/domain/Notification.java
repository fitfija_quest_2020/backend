package com.fitfija.quest.backend.model.domain;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.ManyToOne;

@Entity
@Getter
@Setter
@NoArgsConstructor
public class Notification extends AuditingEntity {

    public Notification(String text, User receiver) {
        this.text = text;
        this.receiver = receiver;
        this.status = NotificationStatus.NEW;
    }

    private String text;

    @ManyToOne
    private User receiver;

    @Enumerated(EnumType.STRING)
    private NotificationStatus status;
}
