package com.fitfija.quest.backend.model.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class UserDto extends AbstractDto {
    private String email;

    private Long invitationId;

    private Integer points;

    private String firstName;

    private String lastName;

    private String faculty;

    private String contactInfo;

    private Long teamId;

    private Boolean isInAcadem;
}
