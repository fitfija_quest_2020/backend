package com.fitfija.quest.backend.model.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class PasswordChangeDto {
    @NotBlank
    @Size(max = 255, min = 6)
    private String oldPassword;
    @NotBlank
    @Size(max = 255, min = 6)
    private String newPassword;
}
