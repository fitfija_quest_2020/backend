package com.fitfija.quest.backend.model.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public abstract class AbstractDto {
    private Long id;
}
