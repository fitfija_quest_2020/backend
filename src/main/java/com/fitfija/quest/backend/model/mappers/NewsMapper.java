package com.fitfija.quest.backend.model.mappers;

import com.fitfija.quest.backend.model.domain.News;
import com.fitfija.quest.backend.model.dto.NewsDto;
import org.springframework.stereotype.Component;

@Component
public class NewsMapper extends AbstractMapper<News, NewsDto> {
    NewsMapper() {
        super(News.class, NewsDto.class);
    }
}
