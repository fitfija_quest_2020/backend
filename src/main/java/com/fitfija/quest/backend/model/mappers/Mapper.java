package com.fitfija.quest.backend.model.mappers;

import com.fitfija.quest.backend.model.domain.AuditingEntity;
import com.fitfija.quest.backend.model.dto.AbstractDto;
import org.modelmapper.ModelMapper;
import org.modelmapper.convention.MatchingStrategies;
import org.springframework.context.annotation.Bean;

import static org.modelmapper.config.Configuration.AccessLevel.PRIVATE;

public interface Mapper<E extends AuditingEntity, D extends AbstractDto> {

    E toEntity(D dto);

    D toDto(E entity);
}
