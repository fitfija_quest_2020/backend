package com.fitfija.quest.backend.model.domain;

public enum InvitationStatus {
    AWAITING, ACCEPTED, REJECTED
}
