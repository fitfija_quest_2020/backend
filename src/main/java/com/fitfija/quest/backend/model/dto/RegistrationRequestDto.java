package com.fitfija.quest.backend.model.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class RegistrationRequestDto {
    @NotBlank
    @Size(max = 255, min = 4)
    private String email;

    @NotBlank
    @Size(max = 255, min = 6)
    private String password;

    @NotBlank
    @Size(max = 255, min = 1)
    private String firstName;

    @NotBlank
    @Size(max = 255, min = 1)
    private String lastName;

    @NotBlank
    @Size(max = 255, min = 1)
    private String contactInfo;

    @NotNull
    private Boolean isInAcadem;
}
