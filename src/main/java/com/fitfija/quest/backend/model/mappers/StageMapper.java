package com.fitfija.quest.backend.model.mappers;

import com.fitfija.quest.backend.model.domain.Stage;
import com.fitfija.quest.backend.model.dto.StageDto;
import org.springframework.stereotype.Component;

@Component
public class StageMapper extends AbstractMapper<Stage, StageDto> {
    StageMapper() {
        super(Stage.class, StageDto.class);
    }
}
