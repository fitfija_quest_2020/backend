package com.fitfija.quest.backend.model.dto;

import com.fitfija.quest.backend.model.domain.Item;
import com.fitfija.quest.backend.model.domain.PurchaseStatus;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class PurchaseDto extends AuditingDto {
    private Item item;

    private PurchaseStatus status;
}
