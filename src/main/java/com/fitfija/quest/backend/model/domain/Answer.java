package com.fitfija.quest.backend.model.domain;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Entity
@Getter
@Setter
public class Answer extends AuditingEntity {

    @ManyToOne
    private Task task;

    private String answer;
}
