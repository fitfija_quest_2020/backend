package com.fitfija.quest.backend.model.mappers;

import com.fitfija.quest.backend.model.domain.Item;
import com.fitfija.quest.backend.model.dto.ItemDto;
import org.springframework.stereotype.Component;

@Component
public class ItemMapper extends AbstractMapper<Item, ItemDto> {
    ItemMapper() {
        super(Item.class, ItemDto.class);
    }
}
