package com.fitfija.quest.backend.model.domain;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Entity;
import javax.validation.constraints.NotNull;

@Getter
@Setter
@Entity
public class Configuration extends AuditingEntity {
    @NotNull
    private Long currentStageId;

    @NotNull
    private Boolean invitationsEnabled;

    @NotNull
    private Integer invitationsLimit;

    @NotNull
    private Boolean teamsEnabled;

    @NotNull
    private Boolean leaderboardEnabled;

    @NotNull
    private Integer leaderboardLimit;

    @NotNull
    private Boolean shopEnabled;

    @NotNull
    private Boolean tasksCanAnswer;

    @NotNull
    private Boolean registrationEnabled;
}
