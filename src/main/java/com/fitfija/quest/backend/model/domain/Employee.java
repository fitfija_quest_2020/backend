package com.fitfija.quest.backend.model.domain;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Entity;

@Getter
@Setter
@Entity
public class Employee extends AuditingEntity {
    private String name;
    private String position;
    private String description;
    private String imageUrl;
}
