package com.fitfija.quest.backend.model.domain;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.ManyToOne;

@Entity
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class Purchase extends AuditingEntity {
    @ManyToOne
    private User buyer;

    @ManyToOne
    private Item item;

    @Enumerated(EnumType.STRING)
    private PurchaseStatus status;
}
