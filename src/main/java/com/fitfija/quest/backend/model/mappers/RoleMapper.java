package com.fitfija.quest.backend.model.mappers;

import com.fitfija.quest.backend.model.domain.Role;
import com.fitfija.quest.backend.model.dto.RoleDto;
import org.springframework.stereotype.Component;

@Component
public class RoleMapper extends AbstractMapper<Role, RoleDto>{
    RoleMapper() {
        super(Role.class, RoleDto.class);
    }
}
