package com.fitfija.quest.backend.model.mappers;

import com.fitfija.quest.backend.exceptions.NotImplementedException;
import com.fitfija.quest.backend.model.domain.Invitation;
import com.fitfija.quest.backend.model.dto.InvitationDto;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

@Component
public class InvitationMapper extends AbstractMapper<Invitation, InvitationDto> {
    InvitationMapper() {
        super(Invitation.class, InvitationDto.class);
    }

    @PostConstruct
    public void setupMapper() {
        mapper.createTypeMap(Invitation.class, InvitationDto.class)
                .addMappings(m -> m.skip(InvitationDto::setReceiverName))
                .addMappings(m -> m.skip(InvitationDto::setSenderName))
                .addMappings(m -> m.skip(InvitationDto::setStatus))
                .setPostConverter(toDtoConverter());
        mapper.createTypeMap(InvitationDto.class, Invitation.class)
                .addMappings(m -> m.skip(Invitation::setSender))
                .addMappings(m -> m.skip(Invitation::setReceiver))
                .addMappings(m -> m.skip(Invitation::setStatus))
                .setPostConverter(toEntityConverter());
    }

    @Override
    public void mapSpecificFields(Invitation source, InvitationDto destination) {
        destination.setReceiverName(source.getReceiver().getFirstName() + " " + source.getReceiver().getLastName());
        destination.setSenderName(source.getSender().getFirstName() + " " + source.getSender().getLastName());
        destination.setStatus(source.getStatus().name());
    }

    @Override
    void mapSpecificFields(InvitationDto source, Invitation destination) {
        // переводить приглашение из dto в entity не планируется

        throw new NotImplementedException();
    }
}
