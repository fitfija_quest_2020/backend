package com.fitfija.quest.backend.model.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class ItemDto extends AuditingDto {
    private String name;

    private String description;

    private Integer inStock;

    private Integer price;

    private String imageUrl;
}
