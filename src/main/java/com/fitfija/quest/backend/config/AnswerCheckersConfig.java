package com.fitfija.quest.backend.config;

import com.fitfija.quest.backend.answercheckers.AnswerChecker;
import com.fitfija.quest.backend.answercheckers.DefaultAnswerChecker;
import com.fitfija.quest.backend.answercheckers.MultipleTriesAnswerChecker;
import com.fitfija.quest.backend.services.ParticipantContextProvider;
import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;

@Configuration
@RequiredArgsConstructor
public class AnswerCheckersConfig {

    private final ParticipantContextProvider participantContextProvider;

    @Bean
    @Primary
    public AnswerChecker getDefaultAnswerChecker() {
        return new DefaultAnswerChecker();
    }

    @Bean("MultipleTriesAnswerChecker")
    public AnswerChecker getMultipleTriesAnswerChecker() {
        return new MultipleTriesAnswerChecker(participantContextProvider);
    }
}