package com.fitfija.quest.backend.config;

import io.swagger.v3.oas.models.Components;
import io.swagger.v3.oas.models.OpenAPI;
import io.swagger.v3.oas.models.info.Info;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class SwaggerConfig  {

    @Bean
    public OpenAPI openAPI(@Value("${fitfija.quest.version}") String appVersion,
                           @Value("${fitfija.quest.title}") String appTitle) {
        return new OpenAPI()
                .components(new Components())
                .info(new Info().title(appTitle).version(appVersion));
    }
}
