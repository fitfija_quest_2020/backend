package com.fitfija.quest.backend.services;

import com.fitfija.quest.backend.api.Messages;
import com.fitfija.quest.backend.exceptions.NotEnabledException;
import com.fitfija.quest.backend.exceptions.NotFoundException;
import com.fitfija.quest.backend.model.domain.*;
import com.fitfija.quest.backend.model.dto.BooleanResultDto;
import com.fitfija.quest.backend.model.dto.ItemDto;
import com.fitfija.quest.backend.model.dto.PurchaseDto;
import com.fitfija.quest.backend.model.dto.ResponseMessage;
import com.fitfija.quest.backend.model.mappers.ItemMapper;
import com.fitfija.quest.backend.model.mappers.PurchaseMapper;
import com.fitfija.quest.backend.repository.ItemRepository;
import com.fitfija.quest.backend.repository.PurchaseRepository;
import com.fitfija.quest.backend.repository.TeamRepository;
import com.fitfija.quest.backend.repository.UserRepository;
import com.fitfija.quest.backend.security.UserManager;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.server.ResponseStatusException;

import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class ShopService {
    private final ItemRepository itemRepository;
    private final PurchaseRepository purchaseRepository;

    private final ConfigurationService configurationService;

    private final UserManager userManager;

    private final ParticipantContextProvider participantContextProvider;

    private final ItemMapper itemMapper;
    private final PurchaseMapper purchaseMapper;

    public List<ItemDto> findAllItems() {
        throwIfNotEnabled();

        return itemRepository.findAll().stream()
                .sorted(Comparator.comparing(Item::getId))
                .map(itemMapper::toDto)
                .collect(Collectors.toList());
    }

    @Transactional
    public ResponseMessage makePurchase(Long id) {
        throwIfNotEnabled();

        Item item = itemRepository.findById(id).orElseThrow(NotFoundException::new);

        if (item.getInStock() == 0) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, Messages.NO_ITEMS_IN_STOCK);
        }

        ParticipantContext participantContext = participantContextProvider.getContext();

        if (participantContext.getPoints() < item.getPrice()) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, Messages.NOT_ENOUGH_POINTS);
        }

        participantContext.subtractPoints(item.getPrice());
        participantContext.save();

        Purchase purchase = new Purchase(userManager.getCurrentUser(), item, PurchaseStatus.IN_TRANSIT);
        purchaseRepository.save(purchase);

        item.setInStock(item.getInStock() - 1);
        itemRepository.save(item);

        return new ResponseMessage(Messages.SUCCESS);
    }

    public List<PurchaseDto> findCurrentUserPurchases() {
        throwIfNotEnabled();

        return purchaseRepository.findAllByBuyer(userManager.getCurrentUser()).stream()
                .sorted(Comparator.comparing(Purchase::getCreated))
                .map(purchaseMapper::toDto)
                .collect(Collectors.toList());
    }

    public BooleanResultDto checkIsEnabled() {
        return new BooleanResultDto(isEnabled());
    }

    private boolean isEnabled() {
        return configurationService.getConfiguration().getShopEnabled();
    }

    private void throwIfNotEnabled() {
        if (!isEnabled()) {
            throw new NotEnabledException();
        }
    }
}
