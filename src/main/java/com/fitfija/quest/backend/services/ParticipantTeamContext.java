package com.fitfija.quest.backend.services;

import com.fitfija.quest.backend.model.domain.Task;
import com.fitfija.quest.backend.model.domain.Team;
import com.fitfija.quest.backend.model.domain.TeamTaskTry;
import com.fitfija.quest.backend.repository.TeamRepository;
import com.fitfija.quest.backend.repository.TeamTaskTryRepository;

public class ParticipantTeamContext implements ParticipantContext {

    private final TeamRepository teamRepository;
    private final TeamTaskTryRepository teamTaskTryRepository;

    private final Team currentTeam;

    public ParticipantTeamContext(TeamRepository teamRepository, TeamTaskTryRepository teamTaskTryRepository, Team currentTeam) {
        this.teamRepository = teamRepository;
        this.teamTaskTryRepository = teamTaskTryRepository;
        this.currentTeam = currentTeam;
    }

    @Override
    public int getPoints() {
        return currentTeam.getCurrentPoints();
    }

    @Override
    public void addPoints(int pointsToAdd) {
        currentTeam.addPoints(pointsToAdd);
    }

    @Override
    public void subtractPoints(int pointsToSubtract) {
        currentTeam.subtractPoints(pointsToSubtract);
    }

    @Override
    public String getName() {
        return currentTeam.getName();
    }

    @Override
    public boolean isTaskCompleted(Task task) {
        return currentTeam.getCompletedTasks().contains(task);
    }

    @Override
    public void incrementTaskTries(Task task) {
        TeamTaskTry teamTaskTry = teamTaskTryRepository.findByTeamAndTask(currentTeam, task)
                .orElseGet(() -> new TeamTaskTry(currentTeam, task));

        teamTaskTry.increment();
        teamTaskTryRepository.save(teamTaskTry);
    }

    @Override
    public void setTaskTries(Task task, int tries) {
        TeamTaskTry teamTaskTry = teamTaskTryRepository.findByTeamAndTask(currentTeam, task)
                .orElseGet(() -> new TeamTaskTry(currentTeam, task));

        teamTaskTry.setTries(tries);
        teamTaskTryRepository.save(teamTaskTry);
    }

    @Override
    public int getTaskTries(Task task) {
        return teamTaskTryRepository.findByTeamAndTask(currentTeam, task)
                .orElseGet(() -> new TeamTaskTry(currentTeam, task))
                .getTries();
    }

    @Override
    public void addCompletedTask(Task task) {
        currentTeam.getCompletedTasks().add(task);
    }

    @Override
    public void save() {
        teamRepository.save(currentTeam);
    }
}
