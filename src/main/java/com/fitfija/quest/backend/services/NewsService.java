package com.fitfija.quest.backend.services;

import com.fitfija.quest.backend.model.domain.AuditingEntity;
import com.fitfija.quest.backend.model.dto.NewsDto;
import com.fitfija.quest.backend.model.mappers.NewsMapper;
import com.fitfija.quest.backend.repository.NewsRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class NewsService {
    private final NewsRepository newsRepository;
    private final NewsMapper newsMapper;

    public List<NewsDto> findAll() {
        return newsRepository.findAll().stream()
                .sorted(Comparator.comparing(AuditingEntity::getCreated).reversed())
                .map(newsMapper::toDto)
                .collect(Collectors.toList());
    }
}
