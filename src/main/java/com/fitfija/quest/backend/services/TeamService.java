package com.fitfija.quest.backend.services;

import com.fitfija.quest.backend.api.Messages;
import com.fitfija.quest.backend.exceptions.NotEnabledException;
import com.fitfija.quest.backend.model.domain.*;
import com.fitfija.quest.backend.model.dto.*;
import com.fitfija.quest.backend.model.mappers.TeamMapper;
import com.fitfija.quest.backend.repository.*;
import com.fitfija.quest.backend.security.UserManager;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.server.ResponseStatusException;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class TeamService {
    private final TeamRepository teamRepository;
    private final TeamTypeRepository teamTypeRepository;
    private final UserRepository userRepository;
    private final InvitationRepository invitationRepository;
    private final NotificationRepository notificationRepository;

    private final UserManager userManager;

    private final TeamMapper teamMapper;

    private final ConfigurationService configurationService;

    private final TeamRandomGenerator teamRandomGenerator;

    public TeamDto findCurrentUserTeam() {
        throwIfNotEnabled();

        return teamMapper.toDto(getCurrentUserTeam().orElseThrow(
                () -> new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, Messages.USER_HAS_NO_TEAM)));
    }

    public ResponseMessage updateCurrentUserTeam(UpdateTeamDto updateTeamDto) {
        throwIfNotEnabled();

        Team team = getCurrentUserTeam().orElseThrow(
                () -> new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, Messages.USER_HAS_NO_TEAM));
        team.setName(updateTeamDto.getName());
        teamRepository.save(team);

        return new ResponseMessage(Messages.SUCCESS);
    }

    public List<TeamDto> getTop(Integer limit) {
        return teamRepository.findAllByIsHiddenFalseAndTotalPointsGreaterThanOrderByTotalPointsDesc(0)
                .stream()
                .limit(limit)
                .map(teamMapper::toDto)
                .collect(Collectors.toList());
    }

    public List<TeamDto> generateFromInvitations() {

        // приглашения можно слать только с разных факультетов, а проект писался для квеста ФИТФИЯ
        // по-хорошему нужно было делать отдульную реализацию сервиса
        TeamType type = teamTypeRepository.findByName("ФИТФИЯ")
                .orElseThrow(() -> new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, Messages.TEAM_TYPE_NOT_FOUND));

        List<Invitation> invitations = invitationRepository.findAllByStatus(InvitationStatus.ACCEPTED);

        List<TeamDto> generatedTeamsList = new ArrayList<>();

        for (Invitation invitation : invitations) {

            // если вдруг команда уже есть, ничего не делаем
            if (invitation.getSender().getTeam() == null) {
                User sender = invitation.getSender();
                User receiver = invitation.getReceiver();

                String name = sender.getFirstName() + " и " + receiver.getFirstName();
                Team team = new Team(name, type);

                sender.setTeam(team);
                receiver.setTeam(team);

                teamRepository.save(team);
                userRepository.save(sender);
                userRepository.save(receiver);

                generatedTeamsList.add(teamMapper.toDto(team));
            }
        }

        return generatedTeamsList;
    }

    @Transactional
    public List<TeamDto> generateRandom() {
        List<Team> generatedTeams = teamRandomGenerator
                .generateTeams(userRepository.findAllByIsHiddenFalseAndTeamIsNullAndStatus(UserStatus.ACTIVE));

        for (Team team : generatedTeams) {
            teamRepository.save(team);

            // знаем, что в комане всегда два пользователя
            // плохой код

            Iterator<User> playersIterator = team.getPlayers().iterator();

            User user1 = playersIterator.next();
            User user2 = playersIterator.next();

            user1.setTeam(team);
            user2.setTeam(team);

            userRepository.save(user1);
            userRepository.save(user2);

            notificationRepository.save(new Notification("It's a match! Теперь " + user2.getFirstName() + " "
                    + user2.getLastName() + " с вами в команде!", user1));
            notificationRepository.save(new Notification("It's a match! Теперь " + user1.getFirstName() + " "
                    + user1.getLastName() + " с вами в команде!", user2));
        }

        return generatedTeams
                .stream()
                .map(teamMapper::toDto)
                .collect(Collectors.toList());
    }

    public BooleanResultDto checkIsEnabled() {
        return new BooleanResultDto(isEnabled());
    }

    private boolean isEnabled() {
        return configurationService.getConfiguration().getTeamsEnabled();
    }

    private Optional<Team> getCurrentUserTeam() {
        User currentUser = userManager.getCurrentUser();

        if (currentUser.getTeam() != null) {
            return Optional.of(currentUser.getTeam());
        } else {
            return Optional.empty();
        }
    }

    private void throwIfNotEnabled() {
        if (!isEnabled()) {
            throw new NotEnabledException();
        }
    }
}
