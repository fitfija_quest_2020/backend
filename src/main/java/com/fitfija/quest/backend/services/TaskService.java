package com.fitfija.quest.backend.services;

import com.fitfija.quest.backend.answercheckers.AnswerChecker;
import com.fitfija.quest.backend.api.Messages;
import com.fitfija.quest.backend.exceptions.NotFoundException;
import com.fitfija.quest.backend.model.domain.*;
import com.fitfija.quest.backend.model.dto.AnswerTryDto;
import com.fitfija.quest.backend.model.dto.AnswerTryResponseDto;
import com.fitfija.quest.backend.model.dto.BooleanResultDto;
import com.fitfija.quest.backend.model.dto.TaskDto;
import com.fitfija.quest.backend.model.mappers.TaskMapper;
import com.fitfija.quest.backend.repository.*;
import com.fitfija.quest.backend.security.UserManager;
import lombok.RequiredArgsConstructor;
import org.springframework.context.ApplicationContext;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

@Service
@RequiredArgsConstructor
public class TaskService {
    //TODO: добавить интерфейсы

    private final TaskRepository taskRepository;

    private final UserManager userManager;

    private final ParticipantContextProvider participantContextProvider;

    private final TaskMapper taskMapper;

    private final ConfigurationService configurationService;

    private final ApplicationContext applicationContext;

    private final AnswerChecker defaultAnswerChecker;

    public TaskDto find(Long id) {
        Task task = taskRepository.getById(id).orElseThrow(NotFoundException::new);

        throwIfUserCannotViewTask(task);

        return taskMapper.toDto(taskRepository.getById(id).orElseThrow(NotFoundException::new));
    }

    private boolean checkAnswer(Task task, String answerToCheck) {
        for (Answer correctAnswer : task.getAnswers()) {
            if (answerToCheck.equalsIgnoreCase(correctAnswer.getAnswer())) {
                return true;
            }
        }

        // если в базе для задания нет ни одного ответа, то любая попытка будет неуспешной

        return false;
    }

    public BooleanResultDto checkIsAnsweringEnabled() {
        return new BooleanResultDto(isAnsweringEnabled());
    }

    private Boolean isAnsweringEnabled() {
        return configurationService.getConfiguration().getTasksCanAnswer();
    }

    public AnswerTryResponseDto complete(Long id, AnswerTryDto answerTryDto) {
        throwIfAnsweringIsDisabled();

        Task task = taskRepository.getById(id).orElseThrow(NotFoundException::new);

        throwIfUserCannotViewTask(task);

        ParticipantContext participantContext = participantContextProvider.getContext();

        if (participantContext.isTaskCompleted(task)) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, Messages.TASK_ALREADY_COMPLETED);
        }

        participantContext.incrementTaskTries(task);

        // для задания может быть задан специальный бин с особой логикой проверки ответа

        AnswerChecker answerChecker;

        if (task.getAnswerCheckerBean() == null) {
            answerChecker = defaultAnswerChecker;
        } else {
            answerChecker = (AnswerChecker) applicationContext.getBean(task.getAnswerCheckerBean());
        }

        if (answerChecker.checkAnswer(task, answerTryDto.getAnswer())) {
            participantContext.addCompletedTask(task);
            participantContext.addPoints(task.getPoints());

            participantContext.save();

            return new AnswerTryResponseDto(true);
        }

        return new AnswerTryResponseDto(false);
    }

    private void throwIfUserCannotViewTask(Task task) {
        Stage stage = task.getStage();

        if (stage.hasEnded()) {
            throw new ResponseStatusException(HttpStatus.FORBIDDEN, Messages.STAGE_ENDED);
        }

        if (!stage.hasStarted()) {
            throw new ResponseStatusException(HttpStatus.FORBIDDEN, Messages.STAGE_NOT_STARTED);
        }

        User user = userManager.getCurrentUser();

        if (configurationService.getConfiguration().getTeamsEnabled() && task.getTeamType() != null &&
                (user.getTeam() == null || !user.getTeam().getType().equals(task.getTeamType()))) {
            throw new ResponseStatusException(HttpStatus.FORBIDDEN, Messages.TASK_IS_FOR_DIFFERENT_TEAM_TYPE);
        }

        if (!configurationService.getConfiguration().getTeamsEnabled() &&
                task.getFaculty() != null && !user.getFaculty().equals(task.getFaculty())) {
            throw new ResponseStatusException(HttpStatus.FORBIDDEN, Messages.TASK_IS_FOR_DIFFERENT_FACULTY);
        }
    }

    private void throwIfAnsweringIsDisabled() {
        if (!configurationService.getConfiguration().getTasksCanAnswer()) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, Messages.TASK_ANSWERING_DISABLED);
        }
    }
}
