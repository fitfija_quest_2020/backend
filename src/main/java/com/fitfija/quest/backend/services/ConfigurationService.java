package com.fitfija.quest.backend.services;

import com.fitfija.quest.backend.api.Messages;
import com.fitfija.quest.backend.model.domain.Configuration;
import com.fitfija.quest.backend.model.dto.ResponseMessage;
import com.fitfija.quest.backend.repository.ConfigurationRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;

@Service
@RequiredArgsConstructor
public class ConfigurationService {
    private final ConfigurationRepository configurationRepository;

    @Value("${fitfija.quest.current-stage-id}")
    private long defaultCurrentStageId;

    @Value("${fitfija.quest.invitations-enabled}")
    private boolean defaultInvitationsEnabled;

    @Value("${fitfija.quest.invitations-limit}")
    private int defaultInvitationsLimit;

    @Value("${fitfija.quest.teams-enabled}")
    private boolean defaultTeamsEnabled;

    @Value("${fitfija.quest.leaderboard-enabled}")
    private boolean defaultLeaderboardEnabled;

    @Value("${fitfija.quest.leaderboard-limit}")
    private int defaultLeaderboardLimit;

    @Value("${fitfija.quest.shop-enabled}")
    private boolean defaultShopEnabled;

    @Value("${fitfija.quest.tasks-can-answer}")
    private boolean defaultTasksCanAnswer;

    @Value("${fifija.quest.registration-enabled}")
    private boolean defaultRegistrationEnabled;

    public Configuration getConfiguration() {
        List<Configuration> configurationList = configurationRepository.findAll();

        if (configurationList.size() > 1) {
            throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, Messages.MULTIPLE_CONFIGURATIONS);
        } else if (configurationList.size() == 1) {
            return configurationList.get(0);
        } else {
            Configuration configuration = new Configuration();

            configuration.setCurrentStageId(defaultCurrentStageId);
            configuration.setInvitationsEnabled(defaultInvitationsEnabled);
            configuration.setInvitationsLimit(defaultInvitationsLimit);
            configuration.setTeamsEnabled(defaultTeamsEnabled);
            configuration.setLeaderboardEnabled(defaultLeaderboardEnabled);
            configuration.setLeaderboardLimit(defaultLeaderboardLimit);
            configuration.setShopEnabled(defaultShopEnabled);
            configuration.setTasksCanAnswer(defaultTasksCanAnswer);
            configuration.setRegistrationEnabled(defaultRegistrationEnabled);

            configurationRepository.save(configuration);

            return configuration;
        }
    }

    public ResponseMessage updateConfiguration(Configuration configuration) {
        Configuration oldConfiguration = getConfiguration();

        configuration.setId(oldConfiguration.getId());
        configuration.setCreated(oldConfiguration.getCreated());

        configurationRepository.save(configuration);

        return new ResponseMessage(Messages.SUCCESS);
    }
}
