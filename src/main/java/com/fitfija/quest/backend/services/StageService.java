package com.fitfija.quest.backend.services;

import com.fitfija.quest.backend.api.Messages;
import com.fitfija.quest.backend.exceptions.NotFoundException;
import com.fitfija.quest.backend.model.domain.AuditingEntity;
import com.fitfija.quest.backend.model.domain.Stage;
import com.fitfija.quest.backend.model.dto.StageDto;
import com.fitfija.quest.backend.model.mappers.StageMapper;
import com.fitfija.quest.backend.repository.StageRepository;
import com.fitfija.quest.backend.repository.UserRepository;
import com.fitfija.quest.backend.security.UserManager;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import java.util.Comparator;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class StageService {
    private final StageRepository stageRepository;

    private final UserManager userManager;

    private final StageMapper stageMapper;

    private final ConfigurationService configurationService;

    public StageDto find(Long id) {
        Stage stage = stageRepository.findById(id).orElseThrow(NotFoundException::new);

        if (stage.hasEnded()) {
            throw new ResponseStatusException(HttpStatus.FORBIDDEN, Messages.STAGE_ENDED);
        }

        if (!stage.hasStarted()) {
            throw new ResponseStatusException(HttpStatus.FORBIDDEN, Messages.STAGE_NOT_STARTED);
        }

        if (configurationService.getConfiguration().getTeamsEnabled()) {
            stage.setTasks(stage.getTasks().stream()
                    .filter(t -> t.getTeamType() == null || (userManager.getCurrentUser().getTeam() != null &&
                            userManager.getCurrentUser().getTeam().getType().equals(t.getTeamType())))
                    .sorted(Comparator.comparing(AuditingEntity::getCreated))
                    .collect(Collectors.toList()));
        } else {
            stage.setTasks(stage.getTasks().stream()
                    .filter(t -> t.getFaculty() == null || userManager.getCurrentUser().getFaculty().equals(t.getFaculty()))
                    .sorted(Comparator.comparing(AuditingEntity::getCreated))
                    .collect(Collectors.toList()));
        }

        return stageMapper.toDto(stageRepository.findById(id).orElseThrow(NotFoundException::new));
    }

    public StageDto findCurrent() {
        return find(configurationService.getConfiguration().getCurrentStageId());
    }
}
