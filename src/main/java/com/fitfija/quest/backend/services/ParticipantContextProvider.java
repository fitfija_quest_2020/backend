package com.fitfija.quest.backend.services;

import com.fitfija.quest.backend.api.Messages;
import com.fitfija.quest.backend.model.domain.User;
import com.fitfija.quest.backend.repository.TeamRepository;
import com.fitfija.quest.backend.repository.TeamTaskTryRepository;
import com.fitfija.quest.backend.repository.UserRepository;
import com.fitfija.quest.backend.repository.UserTaskTryRepository;
import com.fitfija.quest.backend.security.UserManager;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import org.springframework.web.server.ResponseStatusException;

@Component
@RequiredArgsConstructor
public class ParticipantContextProvider {
    private final ConfigurationService configurationService;

    private final UserRepository userRepository;
    private final TeamRepository teamRepository;
    private final UserTaskTryRepository userTaskTryRepository;
    private final TeamTaskTryRepository teamTaskTryRepository;

    private final UserManager userManager;

    public ParticipantContext getContext() {
        User currentUser = userManager.getCurrentUser();

        if (configurationService.getConfiguration().getTeamsEnabled()) {
            if (currentUser.getTeam() == null) {
                throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, Messages.USER_HAS_NO_TEAM);
            }

            return new ParticipantTeamContext(teamRepository, teamTaskTryRepository, currentUser.getTeam());
        }

        return new ParticipantUserContext(userRepository, userTaskTryRepository, currentUser);
    }
}
