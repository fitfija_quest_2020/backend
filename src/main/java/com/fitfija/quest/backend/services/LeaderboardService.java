package com.fitfija.quest.backend.services;

import com.fitfija.quest.backend.exceptions.NotEnabledException;
import com.fitfija.quest.backend.model.domain.Configuration;
import com.fitfija.quest.backend.model.dto.BooleanResultDto;
import com.fitfija.quest.backend.model.dto.LeaderboardEntryDto;
import com.fitfija.quest.backend.model.dto.TeamDto;
import com.fitfija.quest.backend.model.dto.UserDto;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class LeaderboardService {
    private final UserService userService;
    private final TeamService teamService;

    private final ConfigurationService configurationService;

    public List<LeaderboardEntryDto> getLeaderboard(){
        Configuration configuration = configurationService.getConfiguration();

        if (!configuration.getLeaderboardEnabled()) {
            throw new NotEnabledException();
        }

        if (configuration.getTeamsEnabled()) {
            return teamService.getTop(configuration.getLeaderboardLimit())
                    .stream()
                    .map(this::mapTeamDto)
                    .collect(Collectors.toList());
        } else {
            return userService.getTop(configuration.getLeaderboardLimit())
                    .stream()
                    .map(this::mapUserDto)
                    .collect(Collectors.toList());
        }
    }

    public BooleanResultDto checkIsEnabled() {
        return new BooleanResultDto(isEnabled());
    }

    private boolean isEnabled() {
        return configurationService.getConfiguration().getLeaderboardEnabled();
    }

    private LeaderboardEntryDto mapTeamDto(TeamDto teamDto) {
        return new LeaderboardEntryDto(teamDto.getName(), teamDto.getTotalPoints());
    }

    private LeaderboardEntryDto mapUserDto(UserDto userDto) {
        return new LeaderboardEntryDto(userDto.getFirstName() + " " + userDto.getLastName(), userDto.getPoints());
    }
}
