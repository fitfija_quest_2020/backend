package com.fitfija.quest.backend.services;

import com.fitfija.quest.backend.model.domain.Task;
import com.fitfija.quest.backend.model.domain.User;
import com.fitfija.quest.backend.model.domain.UserTaskTry;
import com.fitfija.quest.backend.repository.UserRepository;
import com.fitfija.quest.backend.repository.UserTaskTryRepository;

public class ParticipantUserContext implements ParticipantContext {

    private final UserRepository userRepository;
    private final UserTaskTryRepository userTaskTryRepository;

    private final User currentUser;

    public ParticipantUserContext(UserRepository userRepository, UserTaskTryRepository userTaskTryRepository, User currentUser) {
        this.userRepository = userRepository;
        this.userTaskTryRepository = userTaskTryRepository;
        this.currentUser = currentUser;
    }

    @Override
    public int getPoints() {
        return currentUser.getPoints();
    }

    @Override
    public void addPoints(int pointsToAdd) {
        currentUser.setPoints(currentUser.getPoints() + pointsToAdd);
    }

    @Override
    public void subtractPoints(int pointsToSubtract) {
        if (currentUser.getPoints() - pointsToSubtract < 0) {
            throw new IllegalArgumentException("Cannot make points negative");
        }

        currentUser.setPoints(currentUser.getPoints() - pointsToSubtract);
    }

    @Override
    public String getName() {
        return currentUser.getFirstName() + " " + currentUser.getLastName();
    }

    @Override
    public boolean isTaskCompleted(Task task) {
        return currentUser.getCompletedTasks().contains(task);
    }

    @Override
    public void incrementTaskTries(Task task) {
        UserTaskTry userTaskTry = userTaskTryRepository.findByUserAndTask(currentUser, task)
                .orElseGet(() -> new UserTaskTry(currentUser, task));

        userTaskTry.increment();
        userTaskTryRepository.save(userTaskTry);
    }

    @Override
    public void setTaskTries(Task task, int tries) {
        UserTaskTry userTaskTry = userTaskTryRepository.findByUserAndTask(currentUser, task)
                .orElseGet(() -> new UserTaskTry(currentUser, task));

        userTaskTry.setTries(tries);
        userTaskTryRepository.save(userTaskTry);
    }

    @Override
    public int getTaskTries(Task task) {
        return userTaskTryRepository.findByUserAndTask(currentUser, task)
                .orElseGet(() -> new UserTaskTry(currentUser, task))
                .getTries();
    }

    @Override
    public void addCompletedTask(Task task) {
        currentUser.getCompletedTasks().add(task);
    }

    @Override
    public void save() {
        userRepository.save(currentUser);
    }
}
