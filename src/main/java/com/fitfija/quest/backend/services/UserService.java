package com.fitfija.quest.backend.services;

import com.fitfija.quest.backend.api.Messages;
import com.fitfija.quest.backend.exceptions.JwtAuthenticationException;
import com.fitfija.quest.backend.exceptions.NotEnabledException;
import com.fitfija.quest.backend.model.domain.*;
import com.fitfija.quest.backend.model.dto.*;
import com.fitfija.quest.backend.model.mappers.UserMapper;
import com.fitfija.quest.backend.repository.AllowedEmailRepository;
import com.fitfija.quest.backend.repository.RoleRepository;
import com.fitfija.quest.backend.repository.UserRepository;
import com.fitfija.quest.backend.security.UserManager;
import com.fitfija.quest.backend.security.jwt.JwtTokenProvider;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import java.util.*;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class UserService {
    private final UserRepository userRepository;
    private final RoleRepository roleRepository;
    private final AllowedEmailRepository allowedEmailRepository;

    private final ConfigurationService configurationService;

    private final UserManager userManager;

    private final UserMapper userMapper;

    private final AuthenticationManager authenticationManager;
    private final JwtTokenProvider jwtTokenProvider;

    private final BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();

    private static final Long invitationIdLeftLimit = 1000000000000L;
    private static final Long invitationIdRightLimit = 9999999999999L;

    public void registerUser(RegistrationRequestDto registrationRequest) {

        if (!isRegistrationEnabled()) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, Messages.NOT_ENABLED);
        }

        AllowedEmail allowedEmail = allowedEmailRepository.findByEmail(registrationRequest.getEmail())
                .orElseThrow(() -> new ResponseStatusException(HttpStatus.FORBIDDEN, Messages.EMAIL_NOT_ALLOWED));

        if (!isEmailUnique(registrationRequest.getEmail())) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, Messages.EMAIL_NOT_UNIQUE);
        }

        Long invitationId = generateInvitationId();

        Role roleUser = roleRepository.findByName("ROLE_USER");
        List<Role> userRoles = new ArrayList<>();
        userRoles.add(roleUser);

        User user = new User(
                registrationRequest.getEmail(),
                passwordEncoder.encode(registrationRequest.getPassword()),
                registrationRequest.getFirstName(),
                registrationRequest.getLastName(),
                registrationRequest.getContactInfo(),
                invitationId,
                allowedEmail.getFaculty(),
                registrationRequest.getIsInAcadem(),
                userRoles);

        userRepository.save(user);
    }

    public AuthenticationResponseDto login(AuthenticationRequestDto requestDto) {
        String email = requestDto.getEmail();

        try {
            authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(requestDto.getEmail(), requestDto.getPassword()));
        } catch (BadCredentialsException ex) {
            throw new JwtAuthenticationException(Messages.LOGIN_ERROR, ex);
        }

        User user = userRepository.findByEmail(email).orElseThrow(() -> new JwtAuthenticationException(Messages.LOGIN_ERROR));
        String token = jwtTokenProvider.createToken(email, user.getRoles().stream().map(Role::getName).collect(Collectors.toList()));

        return new AuthenticationResponseDto(email, token);
    }

    public ResponseMessage changePassword(PasswordChangeDto passwordChangeDto) {
        User user = userManager.getCurrentUser();

        throwIfCannotEditUser(user);

        if (!passwordEncoder.matches(passwordChangeDto.getOldPassword(), user.getPassword())) {
            throw new JwtAuthenticationException(Messages.WRONG_PASSWORD);
        }

        user.setPassword(passwordEncoder.encode(passwordChangeDto.getNewPassword()));
        userRepository.save(user);

        return new ResponseMessage(Messages.SUCCESS);
    }

    public ResponseMessage updateUser(UpdateUserDto updateUserDto) {
        User user = userManager.getCurrentUser();

        throwIfCannotEditUser(user);

        user.setFirstName(updateUserDto.getFirstName());
        user.setLastName(updateUserDto.getLastName());
        user.setContactInfo(updateUserDto.getContactInfo());
        user.setIsInAcadem(updateUserDto.getIsInAcadem());

        userRepository.save(user);

        return new ResponseMessage(Messages.SUCCESS);
    }

    // костыль для одного задания, потом уберу
    private void throwIfCannotEditUser(User user) {
        if (user.getId() == 116) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, Messages.CANNOT_EDIT_PROFILE);
        }
    }

    public UserDto findCurrentUser() {
        return userMapper.toDto(userManager.getCurrentUser());
    }

    public UserDto findCurrentUsersTeammate() {
        if (!configurationService.getConfiguration().getTeamsEnabled()) {
            throw new NotEnabledException();
        }

        User currentUser = userManager.getCurrentUser();

        if (currentUser.getTeam() == null) {
            throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, Messages.USER_HAS_NO_TEAM);
        }

        Team team = currentUser.getTeam();

        List<User> notCurrentUserPlayers = team.getPlayers().stream()
                .filter(u -> !u.equals(currentUser))
                .collect(Collectors.toList());

        if (notCurrentUserPlayers.size() > 1) {
            throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, Messages.TEAM_HAS_MORE_THAN_TWO_PLAYERS);
        } else if (notCurrentUserPlayers.size() == 0) {
            throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, Messages.TEAM_HAS_ONLY_ONE_PLAYER);
        }

        return userMapper.toDto(notCurrentUserPlayers.get(0));
    }

    public List<UserDto> getTop(Integer limit) {
        return userRepository.findAllByIsHiddenFalseAndPointsGreaterThanOrderByPointsDesc(0)
                .stream()
                .limit(limit)
                .map(userMapper::toDto)
                .collect(Collectors.toList());
    }

    private Long generateInvitationId() {
        Long invitationId;

        do {
            invitationId = invitationIdLeftLimit + (long) (Math.random() * (invitationIdRightLimit - invitationIdLeftLimit));
        } while (userRepository.findByInvitationId(invitationId).isPresent());

        return invitationId;
    }

    public boolean isEmailUnique(String email) {
        User userWithSameEmail = userRepository.findByEmail(email).orElse(null);

        return userWithSameEmail == null;
    }

    public boolean isEmailAllowed(String email) {
        Optional<AllowedEmail> allowedEmail = allowedEmailRepository.findByEmail(email);

        return allowedEmail.isPresent();
    }

    public BooleanResultDto isCurrentUserAdmin() {
        User currentUser = userManager.getCurrentUser();

        return new BooleanResultDto(currentUser.isAdmin());
    }

    public BooleanResultDto checkIsRegistrationEnabled() {
        return new BooleanResultDto(isRegistrationEnabled());
    }

    private boolean isRegistrationEnabled() {
        return configurationService.getConfiguration().getRegistrationEnabled();
    }
}
