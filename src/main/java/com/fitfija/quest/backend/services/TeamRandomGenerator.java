package com.fitfija.quest.backend.services;

import com.fitfija.quest.backend.api.Messages;
import com.fitfija.quest.backend.model.domain.Team;
import com.fitfija.quest.backend.model.domain.TeamType;
import com.fitfija.quest.backend.model.domain.User;
import com.fitfija.quest.backend.repository.TeamTypeRepository;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import org.springframework.web.server.ResponseStatusException;

import java.util.*;
import java.util.stream.Collectors;

@Component
@RequiredArgsConstructor
public class TeamRandomGenerator {

    private final TeamTypeRepository teamTypeRepository;

    @Getter
    @Setter
    @AllArgsConstructor
    public static class UserPair {
        private User user1;
        private User user2;
        private int distance;

        @Override
        public String toString() {
            return "UserPair{" +
                    "user1=" + user1.getFirstName() + " " + user1.getLastName() + "/" + user1.getFaculty().getName() + "/" + user1.getGender() + "/" + user1.getIsInAcadem() + "/" + user1.getPoints() +
                    ", user2=" + user2.getFirstName() + " " + user2.getLastName() + "/" + user2.getFaculty().getName() + "/" + user2.getGender() + "/" + user2.getIsInAcadem() + "/" + user2.getPoints() +
                    ", distance=" + distance +
                    '}';
        }
    }

    public List<Team> generateTeams(List<User> users) {
        List<Team> teams = new ArrayList<>();
        List<UserPair> pairs = new ArrayList<>();

        // благодаря такой сортировке самые хорошие пары получат те, кто набрал больше баллов и раньше зарегистрировался
        users.sort(Comparator.comparing(User::getPoints).reversed().thenComparing(User::getCreated));

        for (int i = 0; i < users.size(); i++) {
            for (int j = i + 1; j < users.size(); j++) {
                User user1 = users.get(i);
                User user2 = users.get(j);
                pairs.add(new UserPair(user1, user2, distance(user1, user2)));
            }
        }

        pairs.sort(Comparator.comparing(UserPair::getDistance));

        // в реальном продукте такого быть не должно
        TeamType teamTypeFitFija = teamTypeRepository.findByName("ФИТФИЯ")
                .orElseThrow(() -> new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, Messages.TEAM_TYPE_NOT_FOUND));
        TeamType teamTypeFitFit = teamTypeRepository.findByName("ФИТФИТ")
                .orElseThrow(() -> new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, Messages.TEAM_TYPE_NOT_FOUND));

        Set<User> alreadyInTeams = new HashSet<>();

        for (Iterator<UserPair> iterator = pairs.iterator(); iterator.hasNext();) {
            UserPair pair = iterator.next();

            if (alreadyInTeams.contains(pair.user1) | alreadyInTeams.contains(pair.user2)) {
                iterator.remove();
                continue;
            }

            String teamName = pair.user1.getFirstName() + " и " + pair.user2.getFirstName();

            Set<User> players = new HashSet<>();
            players.add(pair.user1);
            players.add(pair.user2);

            // потому что знаем, что фитовцев больше и команд фияфия никогда не будет
            // опять же, это плохой код
            TeamType teamType = pair.user1.getFaculty().equals(pair.user2.getFaculty()) ? teamTypeFitFit : teamTypeFitFija;

            Team team = new Team(teamName, teamType);
            team.setPlayers(players);

            teams.add(team);

            alreadyInTeams.add(pair.user1);
            alreadyInTeams.add(pair.user2);
        }

        return teams;
    }

    private int distance(User user1, User user2) {
        // предпочтительнее участники с разных факультетов
        int distanceFaculty = user1.getFaculty() == user2.getFaculty() ? 1 : 0;

        // предпочтительнее участники, которые могут встретиться
        int distanceAcadem = user1.getIsInAcadem() == user2.getIsInAcadem() ? 0 : 1;

        // предпочтительнее участники разного пола
        int distanceGender = user1.getGender() == user2.getGender() ? 1 : 0;

        // предпочтительнее участники с одинаковыми баллами
        int distancePoints = Math.abs(user1.getPoints() - user2.getPoints());

        // приоритет: факультет, место, пол, баллы (баллы обычно кратны 100)
        return distanceFaculty * 100000 + distanceAcadem * 10000 + distanceGender * 1000 + distancePoints;
    }

    private List<UserPair> removeAllWithUser(List<UserPair> list, User user) {
        return list.stream()
                .filter(pair -> !pair.getUser1().equals(user) && !pair.getUser2().equals(user))
                .collect(Collectors.toList());
    }
}
