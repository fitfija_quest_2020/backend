package com.fitfija.quest.backend.services;

import com.fitfija.quest.backend.api.Messages;
import com.fitfija.quest.backend.exceptions.NotEnabledException;
import com.fitfija.quest.backend.exceptions.NotFoundException;
import com.fitfija.quest.backend.model.domain.Invitation;
import com.fitfija.quest.backend.model.domain.InvitationStatus;
import com.fitfija.quest.backend.model.domain.User;
import com.fitfija.quest.backend.model.dto.BooleanResultDto;
import com.fitfija.quest.backend.model.dto.InvitationDto;
import com.fitfija.quest.backend.model.dto.InvitationsInfoDto;
import com.fitfija.quest.backend.model.dto.NewInvitationRequestDto;
import com.fitfija.quest.backend.model.mappers.InvitationMapper;
import com.fitfija.quest.backend.repository.InvitationRepository;
import com.fitfija.quest.backend.repository.UserRepository;
import com.fitfija.quest.backend.security.UserManager;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class InvitationService {

    private final InvitationRepository invitationRepository;
    private final UserRepository userRepository;

    private final InvitationMapper invitationMapper;

    private final ConfigurationService configurationService;

    private final UserManager userManager;

    public List<InvitationDto> findAllOutbox() {
        throwIfNotEnabled();
        throwIfHaveAccepted();

        User currentUser = userManager.getCurrentUser();

        return invitationRepository.findAllBySender(currentUser).stream()
                .map(invitationMapper::toDto)
                .collect(Collectors.toList());
    }

    public List<InvitationDto> findAllInbox() {
        throwIfNotEnabled();
        throwIfHaveAccepted();

        User currentUser = userManager.getCurrentUser();

        return invitationRepository.findAllByReceiverAndStatus(currentUser, InvitationStatus.AWAITING).stream()
                .map(invitationMapper::toDto)
                .collect(Collectors.toList());
    }

    public void sendInvitation(NewInvitationRequestDto newInvitationRequestDto) {
        throwIfNotEnabled();
        throwIfHaveAccepted();

        int invitationLimit = configurationService.getConfiguration().getInvitationsLimit();

        if (findAllOutbox().stream().filter(i -> !i.getStatus().equals("REJECTED")).count() >= invitationLimit) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, Messages.INVITATION_LIMIT_REACHED);
        }

        User currentUser = userManager.getCurrentUser();
        User receiverUser = userRepository.findByInvitationId(newInvitationRequestDto.getInvitationId())
                .orElseThrow(() -> new NotFoundException(Messages.USER_NOT_FOUND_BY_INVITATION_ID));

        if (currentUser.equals(receiverUser)) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, Messages.CANNOT_SEND_INVITATION_TO_YOURSELF);
        }

        if (currentUser.getFaculty().equals(receiverUser.getFaculty())) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, Messages.CANNOT_SEND_INVITATION_TO_SAME_FACULTY);
        }

        if (invitationRepository.findBySenderAndReceiver(currentUser, receiverUser).isPresent()) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, Messages.ALREADY_SENT_INVITATION);
        }

        if (invitationRepository.findBySenderAndReceiver(receiverUser, currentUser).isPresent()) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, Messages.ALREADY_RECEIVED_INVITATION);
        }

        if (invitationRepository.findByReceiverAndStatus(receiverUser, InvitationStatus.ACCEPTED).isPresent()) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, Messages.RECEIVER_ALREADY_HAS_TEAM);
        }

        if (invitationRepository.findBySenderAndStatus(receiverUser, InvitationStatus.ACCEPTED).isPresent()) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, Messages.RECEIVER_ALREADY_HAS_TEAM);
        }

        Invitation invitation = new Invitation(currentUser, receiverUser, InvitationStatus.AWAITING);

        invitationRepository.save(invitation);
    }

    public void reject(Long id) {
        throwIfNotEnabled();
        throwIfHaveAccepted();

        Invitation invitation = invitationRepository.findById(id).orElseThrow(NotFoundException::new);
        User currentUser = userManager.getCurrentUser();

        if (!invitation.getReceiver().equals(currentUser) || !invitation.getStatus().equals(InvitationStatus.AWAITING)) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, Messages.CANNOT_REJECT_INVITATION);
        }

        invitation.setStatus(InvitationStatus.REJECTED);
        invitationRepository.save(invitation);
    }

    @Transactional
    public void accept(Long id) {
        throwIfNotEnabled();
        throwIfHaveAccepted();

        Invitation invitation = invitationRepository.findById(id).orElseThrow(NotFoundException::new);
        User currentUser = userManager.getCurrentUser();
        User originalSender = invitation.getSender();

        if (!invitation.getReceiver().equals(currentUser) || !invitation.getStatus().equals(InvitationStatus.AWAITING)) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, Messages.CANNOT_ACCEPT_INVITATION);
        }

        invitation.setStatus(InvitationStatus.ACCEPTED);
        invitationRepository.save(invitation);

        // нужно отключить все другие приглашения участников команды

        List<Invitation> senderInbox =
                invitationRepository.findAllByReceiverAndStatus(originalSender, InvitationStatus.AWAITING);
        List<Invitation> senderOutbox =
                invitationRepository.findAllBySenderAndStatus(originalSender, InvitationStatus.AWAITING);
        List<Invitation> receiverInbox =
                invitationRepository.findAllByReceiverAndStatus(currentUser, InvitationStatus.AWAITING);
        List<Invitation> receiverOutbox =
                invitationRepository.findAllBySenderAndStatus(currentUser, InvitationStatus.AWAITING);

        setAllRejected(senderInbox);
        setAllRejected(senderOutbox);
        setAllRejected(receiverInbox);
        setAllRejected(receiverOutbox);
    }

    public BooleanResultDto checkIsEnabled() {
        return new BooleanResultDto(isEnabled());
    }

    public InvitationsInfoDto getInvitationsInfo() {
        Optional<Invitation> acceptedInvitationOptional = findAccepted();

        if (!acceptedInvitationOptional.isPresent()) {
            return new InvitationsInfoDto(false, "");
        } else {
            Invitation acceptedInvitation = acceptedInvitationOptional.get();

            User currentUser = userManager.getCurrentUser();
            User teammate = acceptedInvitation.getReceiver().equals(currentUser) ?
                    acceptedInvitation.getSender() : acceptedInvitation.getReceiver();

            return new InvitationsInfoDto(true, teammate.getFirstName() + " " + teammate.getLastName());
        }
    }

    private boolean isEnabled() {
        return configurationService.getConfiguration().getInvitationsEnabled();
    }

    private void throwIfNotEnabled() {
        if (!isEnabled()) {
            throw new NotEnabledException();
        }
    }

    private void throwIfHaveAccepted() {
        if (findAccepted().isPresent()) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, Messages.INVITATION_ALREADY_ACCEPTED);
        }
    }

    private Optional<Invitation> findAccepted() {
        User currentUser = userManager.getCurrentUser();

        Optional<Invitation> outgoingAccepted =
                invitationRepository.findBySenderAndStatus(currentUser, InvitationStatus.ACCEPTED);
        Optional<Invitation> incomingAccepted =
                invitationRepository.findByReceiverAndStatus(currentUser, InvitationStatus.ACCEPTED);

        if (outgoingAccepted.isPresent()) {
            return outgoingAccepted;
        } else if (incomingAccepted.isPresent()) {
            return incomingAccepted;
        } else {
            // неважно, что возвращать, они оба пустые
            return outgoingAccepted;
        }
    }

    private void setAllRejected(List<Invitation> invitations) {
        for (Invitation invitation : invitations) {
            invitation.setStatus(InvitationStatus.REJECTED);
            invitationRepository.save(invitation);
        }
    }
}
