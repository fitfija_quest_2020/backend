package com.fitfija.quest.backend.services;

import com.fitfija.quest.backend.api.Messages;
import com.fitfija.quest.backend.exceptions.NotFoundException;
import com.fitfija.quest.backend.model.domain.Notification;
import com.fitfija.quest.backend.model.domain.NotificationStatus;
import com.fitfija.quest.backend.model.domain.User;
import com.fitfija.quest.backend.model.dto.NotificationDto;
import com.fitfija.quest.backend.model.dto.NotificationToAllDto;
import com.fitfija.quest.backend.model.dto.ResponseMessage;
import com.fitfija.quest.backend.model.mappers.NotificationMapper;
import com.fitfija.quest.backend.repository.NotificationRepository;
import com.fitfija.quest.backend.repository.UserRepository;
import com.fitfija.quest.backend.security.UserManager;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class NotificationService {
    private final NotificationRepository notificationRepository;
    private final UserRepository userRepository;

    private final UserManager userManager;

    private final NotificationMapper notificationMapper;

    public List<NotificationDto> findAllNewForCurrentUser() {
        User currentUser = userManager.getCurrentUser();

        List<Notification> notifications = notificationRepository
                .findAllByReceiverAndStatus(currentUser, NotificationStatus.NEW);

        return notifications.stream()
                .map(notificationMapper::toDto)
                .collect(Collectors.toList());
    }

    public ResponseMessage markAsSeen(Long id) {
        Notification notification = notificationRepository.findById(id).orElseThrow(NotFoundException::new);

        User currentUser = userManager.getCurrentUser();

        if (!notification.getReceiver().equals(currentUser)) {
            throw new ResponseStatusException(HttpStatus.FORBIDDEN, Messages.NOTIFICATION_AS_SEEN_FORBIDDEN);
        }

        notification.setStatus(NotificationStatus.SEEN);
        notificationRepository.save(notification);

        return new ResponseMessage(Messages.SUCCESS);
    }

    public ResponseMessage sendAll(NotificationToAllDto notificationToAllDto) {
        for (User user : userRepository.findAll()) {
            Notification notification = new Notification(notificationToAllDto.getText(), user);
            notificationRepository.save(notification);
        }

        return new ResponseMessage(Messages.SUCCESS);
    }
}
