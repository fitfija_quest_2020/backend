package com.fitfija.quest.backend.services;

import com.fitfija.quest.backend.model.domain.Task;

public interface ParticipantContext {
    int getPoints();

    void addPoints(int pointsToAdd);

    void subtractPoints(int pointsToSubtract);

    String getName();

    boolean isTaskCompleted(Task task);

    void incrementTaskTries(Task task);

    void setTaskTries(Task task, int tries);

    int getTaskTries(Task task);

    void addCompletedTask(Task task);

    void save();
}
