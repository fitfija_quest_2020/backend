package com.fitfija.quest.backend.security;

import com.fitfija.quest.backend.api.Messages;
import com.fitfija.quest.backend.model.domain.User;
import com.fitfija.quest.backend.repository.UserRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.context.SecurityContextHolderStrategy;
import org.springframework.stereotype.Component;
import org.springframework.web.server.ResponseStatusException;

@Component
@RequiredArgsConstructor
public class UserManager {

    private final UserRepository userRepository;

    private final SecurityContextHolderStrategy securityContextHolderStrategy;

    public User getCurrentUser() {
        return userRepository.findByEmail(securityContextHolderStrategy.getContext().getAuthentication().getName())
                .orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, Messages.USER_DELETED));
    }
}
