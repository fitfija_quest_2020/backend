package com.fitfija.quest.backend.security.jwt;

import com.fitfija.quest.backend.api.Messages;
import com.fitfija.quest.backend.exceptions.JwtAuthenticationException;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.filter.OncePerRequestFilter;
import org.springframework.web.servlet.HandlerExceptionResolver;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class JwtTokenFilter extends OncePerRequestFilter {

    private final JwtTokenProvider jwtTokenProvider;
    private final HandlerExceptionResolver handlerExceptionResolver;

    public JwtTokenFilter(JwtTokenProvider jwtTokenProvider, HandlerExceptionResolver handlerExceptionResolver) {
        super();
        this.jwtTokenProvider = jwtTokenProvider;
        this.handlerExceptionResolver = handlerExceptionResolver;
    }

    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain) {
        try {
            String token = jwtTokenProvider.resolveToken(request);
            if (token != null && jwtTokenProvider.validateToken(token)) {
                Authentication auth = jwtTokenProvider.getAuthentication(token);

                if (auth != null) {
                    UserDetails userDetails = (UserDetails) auth.getPrincipal();
                    if (!userDetails.isEnabled()) {
                        throw new JwtAuthenticationException(Messages.USER_NOT_ACTIVE);
                    }

                    SecurityContextHolder.getContext().setAuthentication(auth);
                }
            }
            filterChain.doFilter(request, response);
        } catch (Exception e) {
            // таким образом ошибки будут обработаны GlobalExceptionHandler, как ошибки из контроллеров

            handlerExceptionResolver.resolveException(request, response, null, e);
        }
    }
}
